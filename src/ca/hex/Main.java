package ca.hex;

import ca.hex.engine.FPSCounter;
import org.lwjgl.glfw.GLFWErrorCallback;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.opengl.GL.createCapabilities;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Main {

    private static final float NANOSECONDS_TO_MILLISECONDS = 1.0f / 1000000.0f;

    private static final float TIME_STEP = 0.01f;
    private static final long TIME_STEP_L = (long) (TIME_STEP * 1000000000.0);
    private static final long MAX_DELTA_TIME = (long) (0.25 * 1000000000.0);

    private static final String WIN_TITLE = "Window Title";
    private static final int WIN_WIDTH = 1280;
    private static final int WIN_HEIGHT = 800;
    private static final boolean WIN_VSYNC = true;

    public static void main(String[] args) {
        //Set error callback
        GLFWErrorCallback.createPrint(System.err).set();

        if (!glfwInit())
            throw new IllegalStateException("GLFW init failed");

        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); //Start hidden
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); //Allow resize
        glfwWindowHint(GLFW_SAMPLES, 4);

        long win = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, WIN_TITLE, NULL, NULL);
        if (win == NULL)
            throw new IllegalStateException("Window creation failed");

        glfwMakeContextCurrent(win);
        glfwSwapInterval(WIN_VSYNC ? 1 : 0); //Set to 0 to remove vsync
        glfwShowWindow(win);

        createCapabilities();

        Game game = new Game();

        int[] winDims = {
                WIN_WIDTH,
                WIN_HEIGHT,
                WIN_WIDTH / 2,
                WIN_HEIGHT / 2
        };

        //resize
        game.onResize(winDims[0], winDims[1]);
        glfwSetWindowSizeCallback(win, (window, width, height) -> {
            winDims[0] = width;
            winDims[1] = height;
            winDims[2] = width / 2;
            winDims[3] = height / 2;
            game.onResize(width, height);
        });

        //input callbacks
        glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
        glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL); // to re enable
        float[] cursorPosDelta = {0, 0};
        glfwSetCursorPosCallback(win, (window, xpos, ypos) -> {
            cursorPosDelta[0] = winDims[2] - (float) xpos;
            cursorPosDelta[1] = winDims[3] - (float) ypos;
            game.onMouseInput(cursorPosDelta[0], cursorPosDelta[1]);
            glfwSetCursorPos(window, winDims[2], winDims[3]);
        });
        glfwSetKeyCallback(win, (window, key, scancode, action, mods) -> {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
                glfwSetWindowShouldClose(window, true);
            } else {
                game.onKeyInput(key, action);
            }
        });

        long newTime;
        long deltaTime;
        long gameTime = 0;
        long accumulator = 0;
        long curTimeStamp = System.nanoTime();

        //Temp
        FPSCounter fpsCounter = new FPSCounter();
        long renderTimer;
        float drawTime;
        int titleUpdater = 0;
        int fps;
        //Temp

        System.gc();
        while (!glfwWindowShouldClose(win)) {
            newTime = System.nanoTime();
            deltaTime = newTime - curTimeStamp;

            if (deltaTime > MAX_DELTA_TIME) {
                deltaTime = MAX_DELTA_TIME;
            }

            curTimeStamp = newTime;
            accumulator += deltaTime;

            while (accumulator >= TIME_STEP_L) {
                game.onUpdate(TIME_STEP);
                accumulator -= TIME_STEP_L;
                gameTime += TIME_STEP_L;
            }

            renderTimer = System.nanoTime();
            game.onRender((float)(accumulator / TIME_STEP_L));

            fps = fpsCounter.onTick();
            if (titleUpdater++ == 100) {
                titleUpdater = 0;
                drawTime = (System.nanoTime() - renderTimer) * NANOSECONDS_TO_MILLISECONDS;
                glfwSetWindowTitle(win, "FPS: " + fps + " Draw Time: " + String.format("%.4f", drawTime) + " (ms)");
            }

            glfwSwapBuffers(win);
            glfwPollEvents();
        }

        game.onDestroy();

        glfwDestroyWindow(win);
        glfwTerminate();
    }
}
