package ca.hex;

import ca.hex.engine.test_new.*;
import ca.hex.engine.test_new.gbuffer.GBuffer;
import ca.hex.engine.test_new.gl.*;
import ca.hex.engine.test_new.model.Mesh;
import ca.hex.engine.test_new.model.Model;
import ca.hex.engine.test_new.model.ModelLoader;
import ca.hex.engine.test_new.Log;
import ca.hex.engine.test_new.utils.ShaderUtils;
import ca.hex.engine.FileUtils;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import static ca.hex.engine.test_new.utils.BufferUtils.getFloatBuffer;
import static ca.hex.engine.test_new.utils.BufferUtils.getIntBuffer;
import static ca.hex.f_math.Mat.*;
import static ca.hex.f_math.Mat.multiplyMM;
import static ca.hex.f_math.Vec3.*;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ENTER;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

public final class Game {


    private static final int MAX_DIRECTIONAL_LIGHTS = 5;
    private static final int MAX_POINT_LIGHTS = 5;
    private static final int MAX_SPOT_LIGHTS = 5;
    private float[] TEMP_LVM = new float[16];

    private static class DirectionalLight {
        final float[] Direction = zero(new float[3]);
        final float[] Color = one(new float[3]);
    }

    private static class PointLight {
        final float[] Position = zero(new float[3]);
        final float[] Color = one(new float[3]);
    }

    private static class SpotLight {
        final float[] Position = zero(new float[3]);
        final float[] Direction = zero(new float[3]);
        final float[] Color = one(new float[3]);
    }

    private static final Mesh PP_QUAD;
    static {
        FloatBuffer vertexBuffer = getFloatBuffer(4 * 3);
        FloatBuffer texcoordBuffer = getFloatBuffer(4 * 2);
        IntBuffer indexBuffer = getIntBuffer(4);
        vertexBuffer.put(-1);
        vertexBuffer.put(-1);
        vertexBuffer.put( 1);
        texcoordBuffer.put(0);
        texcoordBuffer.put(0);
        indexBuffer.put(0);

        vertexBuffer.put( 1);
        vertexBuffer.put(-1);
        vertexBuffer.put( 1);
        texcoordBuffer.put(1);
        texcoordBuffer.put(0);
        indexBuffer.put(1);

        vertexBuffer.put( 1);
        vertexBuffer.put( 1);
        vertexBuffer.put( 1);
        texcoordBuffer.put(1);
        texcoordBuffer.put(1);
        indexBuffer.put(2);

        vertexBuffer.put(-1);
        vertexBuffer.put( 1);
        vertexBuffer.put( 1);
        texcoordBuffer.put(0);
        texcoordBuffer.put(1);
        indexBuffer.put(3);

        vertexBuffer.flip();
        texcoordBuffer.flip();
        indexBuffer.flip();

        PP_QUAD = new Mesh.Builder()
                .setVertices(vertexBuffer)
                .setTexcoords(texcoordBuffer)
                .setIndices(indexBuffer, 4)
                .setMode(GL_TRIANGLE_FAN)
                .build();
    }

    private static final int SHADER;
    private static final int SHADER_VIEWPOS;

    private static final int SHADER_DIRECTIONAL_LIGHTS_ACTIVE;
    private static final int SHADER_POINT_LIGHTS_ACTIVE;
    private static final int SHADER_SPOT_LIGHTS_ACTIVE;

    private static final int[] SHADER_DIRECTIONAL_LIGHT_DIRECTIONS = new int [MAX_DIRECTIONAL_LIGHTS];
    private static final int[] SHADER_DIRECTIONAL_LIGHT_COLORS = new int [MAX_DIRECTIONAL_LIGHTS];

    private static final int[] SHADER_POINT_LIGHT_POSITIONS = new int [MAX_POINT_LIGHTS];
    private static final int[] SHADER_POINT_LIGHT_COLORS = new int [MAX_POINT_LIGHTS];

    private static final int[] SHADER_SPOT_LIGHT_POSITIONS = new int [MAX_SPOT_LIGHTS];
    private static final int[] SHADER_SPOT_LIGHT_DIRECTIONS = new int [MAX_SPOT_LIGHTS];
    private static final int[] SHADER_SPOT_LIGHT_COLORS = new int [MAX_SPOT_LIGHTS];

    private static int testShadowSpaceMatrix;

    static {
        String vs = FileUtils.readFileToString("src/res/shaders/postprocessing/pp_base.vert");
        String fs = FileUtils.readFileToString("src/res/shaders/postprocessing/pp_lit.frag");
        //fs = FileUtils.readFileToString("src/res/shaders/postprocessing/test.frag");

        SHADER = ShaderUtils.load(vs, fs);
        SHADER_VIEWPOS = glGetUniformLocation(SHADER, "uViewPosition");

        SHADER_DIRECTIONAL_LIGHTS_ACTIVE = glGetUniformLocation(SHADER, "uDirectionalLightsActive");
        SHADER_POINT_LIGHTS_ACTIVE = glGetUniformLocation(SHADER, "uPointLightsActive");
        SHADER_SPOT_LIGHTS_ACTIVE = glGetUniformLocation(SHADER, "uSpotLightsActive");

        for (int i = 0; i < MAX_DIRECTIONAL_LIGHTS; i++) {
            SHADER_DIRECTIONAL_LIGHT_DIRECTIONS[i] = glGetUniformLocation(SHADER, "uDirLights[" + i + "].Direction");
            SHADER_DIRECTIONAL_LIGHT_COLORS[i] = glGetUniformLocation(SHADER, "uDirLights[" + i + "].Color");
        }

        for (int i = 0; i < MAX_POINT_LIGHTS; i++) {
            SHADER_POINT_LIGHT_POSITIONS[i] = glGetUniformLocation(SHADER, "uPointLights[" + i + "].Position");
            SHADER_POINT_LIGHT_COLORS[i] = glGetUniformLocation(SHADER, "uPointLights[" + i + "].Color");
        }

        for (int i = 0; i < MAX_SPOT_LIGHTS; i++) {
            SHADER_SPOT_LIGHT_POSITIONS[i] = glGetUniformLocation(SHADER, "uSpotLights[" + i + "].Position");
            SHADER_SPOT_LIGHT_DIRECTIONS[i] = glGetUniformLocation(SHADER, "uSpotLights[" + i + "].Direction");
            SHADER_SPOT_LIGHT_COLORS[i] = glGetUniformLocation(SHADER, "uSpotLights[" + i + "].Color");
        }

        testShadowSpaceMatrix = glGetUniformLocation(SHADER, "testShadowSpaceMatrix");

        glUseProgram(SHADER);
        glUniform1i(glGetUniformLocation(SHADER, "uPosition"),          0);
        glUniform1i(glGetUniformLocation(SHADER, "uNormal"),            1);
        glUniform1i(glGetUniformLocation(SHADER, "uAlbedoRoughness"),   2);
        glUniform1i(glGetUniformLocation(SHADER, "uEmissionMetallic"),  3);

        glUniform1i(glGetUniformLocation(SHADER, "shadowMap"),          4);
        glUseProgram(0);
    }


    private ArrayList<DirectionalLight> mDirectionalLights = new ArrayList<>();
    private ArrayList<PointLight> mPointLights = new ArrayList<>();
    private ArrayList<SpotLight> mSpotLights = new ArrayList<>();

    private void pushLightsToShader() {
        glUniform1i(SHADER_DIRECTIONAL_LIGHTS_ACTIVE, mDirectionalLights.size());
        glUniform1i(SHADER_POINT_LIGHTS_ACTIVE, mPointLights.size());
        glUniform1i(SHADER_SPOT_LIGHTS_ACTIVE, mSpotLights.size());

        for (int i = 0; i < mDirectionalLights.size(); i++) {
            DirectionalLight light = mDirectionalLights.get(i);

            GLDebug.drawPoint(zero(new float[3]), light.Color[0], light.Color[1], light.Color[2]);
            GLDebug.drawLine(zero(new float[3]), light.Direction, light.Color[0], light.Color[1], light.Color[2]);

            glUniform3fv(SHADER_DIRECTIONAL_LIGHT_DIRECTIONS[i], light.Direction);
            glUniform3fv(SHADER_DIRECTIONAL_LIGHT_COLORS[i], light.Color);
        }

        for (int i = 0; i < mPointLights.size(); i++) {
            PointLight light = mPointLights.get(i);

            GLDebug.drawPoint(light.Position, light.Color[0], light.Color[1], light.Color[2]);

            glUniform3fv(SHADER_POINT_LIGHT_POSITIONS[i], light.Position);
            glUniform3fv(SHADER_POINT_LIGHT_COLORS[i], light.Color);
        }

        for (int i = 0; i < mSpotLights.size(); i++) {
            SpotLight light = mSpotLights.get(i);

            GLDebug.drawPoint(light.Position, light.Color[0], light.Color[1], light.Color[2]);

            glUniform3fv(SHADER_SPOT_LIGHT_POSITIONS[i], light.Position);
            glUniform3fv(SHADER_SPOT_LIGHT_DIRECTIONS[i], light.Direction);
            glUniform3fv(SHADER_SPOT_LIGHT_COLORS[i], light.Color);
        }
    }

    private void randomizeLights() {
        int dirCnt = (int) (Math.random() * MAX_DIRECTIONAL_LIGHTS);
        int pointCnt = (int) (Math.random() * MAX_DIRECTIONAL_LIGHTS);

        mDirectionalLights.clear();
        for (int i = 0; i < dirCnt; i++) {
            DirectionalLight light = new DirectionalLight();
            light.Direction[0] = (float) (Math.random() * 20 - 10);
            light.Direction[1] = (float) (Math.random() * 20 - 10);
            light.Direction[2] = (float) (Math.random() * 20 - 10);
            norm(light.Direction, light.Direction);
            light.Color[0] = (float) Math.random();
            light.Color[1] = (float) Math.random();
            light.Color[2] = (float) Math.random();
            mDirectionalLights.add(light);
        }

        mPointLights.clear();
        for (int i = 0; i < pointCnt; i++) {
            PointLight light = new PointLight();
            light.Position[0] = (float) (Math.random() * 20 - 10);
            light.Position[1] = (float) (Math.random() * 20 - 10);
            light.Position[2] = (float) (Math.random() * 20 - 10);
            light.Color[0] = (float) Math.random();
            light.Color[1] = (float) Math.random();
            light.Color[2] = (float) Math.random();
            mPointLights.add(light);
        }
    }

    private int mWidth = 800;
    private int mHeight = 600;

    //private TestCamera mCamera;
    private Camera mCamera;
    private Skybox mSkybox;
    private Model mModel;

    private GBuffer mGBuffer;

    private Camera mLightCamera;
    private Framebuffer mDepthMap;

    private Shader mDepthShader;

    public Game() {
        Log.d("GL_VERSION: " + glGetString(GL_VERSION));

        //glEnable(GL_MULTISAMPLE);

        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);

        //glEnable(GL_BLEND);
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        mCamera = new Camera();

        Cubemap skyboxCubemap = new Cubemap(
                "src/res/textures/skybox/right.jpg",
                "src/res/textures/skybox/left.jpg",
                "src/res/textures/skybox/top.jpg",
                "src/res/textures/skybox/bottom.jpg",
                "src/res/textures/skybox/front.jpg",
                "src/res/textures/skybox/back.jpg"
        );
        mSkybox = new Skybox(skyboxCubemap);

        mModel = ModelLoader.load("src/res/models/buster-drone/BusterDrone.fbx");
        //mModel = ModelLoader.load("src/res/models/drossel/Drossel.fbx");
        //mModel = ModelLoader.load("src/res/models/stormtrooper/stormtrooper.fbx");
        //mModel = ModelLoader.load("src/res/models/vanille/happy.fbx");
        mModel.setScale(1/10f);

        mGBuffer = new GBuffer();

        mLightCamera = new Camera();

        orthoM(mLightCamera.getProjectionMatrix(), -10.0f, 10.0f, -10.0f, 10.0f, 0, 30f);
        setLookAtM(mLightCamera.getViewMatrix(), new float[] {-5.0f, 10.0f, -5.0f}, new float[] {0, 0, 0}, new float[] {0, 1, 0});
        multiplyMM(mLightCamera.getViewProjectionMatrix(), mLightCamera.getProjectionMatrix(), mLightCamera.getViewMatrix());
        mLightCamera.getPosition()[0] = -5;
        mLightCamera.getPosition()[1] = 10;
        mLightCamera.getPosition()[2] = -5;

        mDepthMap = new Framebuffer(
                new Framebuffer.Attachment(GL_DEPTH_ATTACHMENT,
                        new Texture2D(2048, 2048, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_FLOAT)
                )
        );

        mDepthShader = new Shader(ShaderUtils.load(
                FileUtils.readFileToString("src/res/shaders/depth/depth.vert"),
                FileUtils.readFileToString("src/res/shaders/depth/depth.frag")
        ));
    }

    public void onResize(int width, int height) {
        mWidth = width;
        mHeight = height;

        glViewport(0, 0, width, height);
        mCamera.onResize(width, height);
        mGBuffer.onResize(width, height);
    }

    private boolean mIsPaused = false;
    private float mGlobalTime = 0;

    public void onUpdate(float timeStep) {
        mCamera.onUpdate(timeStep);

        if (!mIsPaused) {
            mGlobalTime += timeStep;
            mModel.animate(0, mGlobalTime);
        }

    }

    public void onKeyInput(int key, int action) {
        if (key == GLFW_KEY_SPACE &&
            action == GLFW_PRESS) {
            mIsPaused ^= true;
        } else
        if (key == GLFW_KEY_ENTER &&
            action == GLFW_PRESS) {
            randomizeLights();
        } else {
            mCamera.onKeyInput(key, action);
        }
    }

    public void onMouseInput(float deltaX, float deltaY) {
        mCamera.onMouseInput(deltaX, deltaY);
    }

    public void onRender(float alpha) {
        glEnable(GL_DEPTH_TEST);
        mCamera.onRender(alpha);

        //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //mModel.render(mLightCamera.getViewProjectionMatrix(), mDepthShader);
//*
        glViewport(0, 0, 2048, 2048);

        //set viewport to depthmap dims
        glBindFramebuffer(GL_FRAMEBUFFER, mDepthMap.getId());
        glClear(GL_DEPTH_BUFFER_BIT);

        //mModel.setPosition(0, 0, 0);
        mModel.render(mCamera.getViewProjectionMatrix(), mDepthShader);
        //mModel.setPosition(20, 0, 20);
        //mModel.render(mCamera.getViewProjectionMatrix(), mDepthShader);


        //reset viewport to screen dims
        glViewport(0, 0, mWidth, mHeight);

        //render to gbuffer
        glBindFramebuffer(GL_FRAMEBUFFER, mGBuffer.getId());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        //mModel.setPosition(0, 0, 0);
        mModel.render(mCamera.getViewProjectionMatrix(), mGBuffer.getShader());
        //mModel.setPosition(20, 0, 20);
        //mModel.render(mCamera.getViewProjectionMatrix(), mGBuffer.getShader());
        mSkybox.render(mCamera);

        //render to screen
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        glUseProgram(SHADER);

        glUniformMatrix4fv(testShadowSpaceMatrix, false, mLightCamera.getViewProjectionMatrix());

        pushLightsToShader();
        glUniform3fv(SHADER_VIEWPOS, mCamera.getPosition());
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mGBuffer.getPositionAttachment());
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, mGBuffer.getNormalAttachment());
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, mGBuffer.getColorRoughnessAttachment());
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, mGBuffer.getEmissionMetallicAttachment());

        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, mDepthMap.getAttachment(GL_DEPTH_ATTACHMENT).getId());

        PP_QUAD.render();

        glBindTexture(GL_TEXTURE_2D, 0);
        glUseProgram(0);
//*/
        GLDebug.drawDebugging(mCamera.getProjectionMatrix(), mCamera.getViewMatrix());
    }

    public void onDestroy() {
        mModel.destroy();
        mSkybox.destroy();
        mGBuffer.destroy();
        mDepthMap.destroy();
    }
}








