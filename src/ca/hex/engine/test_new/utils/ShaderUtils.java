package ca.hex.engine.test_new.utils;

import ca.hex.engine.test_new.Log;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;

public final class ShaderUtils {
    private ShaderUtils() { }

    //TODO cache shaders and if unused for compile after a while, clean up(delete).

    public static int load(
            String vertexSource,
            String fragmentSource
    ) {
        int vs = createShader(GL_VERTEX_SHADER, vertexSource);
        int fs = createShader(GL_FRAGMENT_SHADER, fragmentSource);
        return createProgram(vs, fs);
    }

    public static int load(
            String vertexSource,
            String geometrySource,
            String fragmentSource
    ) {
        int vs = createShader(GL_VERTEX_SHADER, vertexSource);
        int gs = createShader(GL_GEOMETRY_SHADER, geometrySource);
        int fs = createShader(GL_FRAGMENT_SHADER, fragmentSource);
        return createProgram(vs, gs, fs);
    }

    private static int createShader(int type, String source) {
        int shader = glCreateShader(type);
        if (shader != 0) {
            glShaderSource(shader, source);
            glCompileShader(shader);

            if (glGetShaderi(shader, GL_COMPILE_STATUS) == GL_FALSE) {
                final String shaderType;
                switch (type) {
                    case GL_VERTEX_SHADER:   shaderType = "Vertex";   break;
                    case GL_FRAGMENT_SHADER: shaderType = "Fragment"; break;
                    default: shaderType = "(INVALID_TYPE: " + type + ")";
                }
                Log.e(shaderType + " compile error:\n" +
                        glGetShaderInfoLog(shader));
                glDeleteShader(shader);
                shader = 0;
            }
        }
        return shader;
    }

    private static int createProgram(int... shaders) {
        int program = glCreateProgram();
        if (program != 0) {
            for (int shader : shaders) {
                glAttachShader(program, shader);
            }

            //TODO - some way to clean this up and generate it to match the mesh format.
            glBindAttribLocation(program, 0, "aPosition");
            glBindAttribLocation(program, 1, "aNormal");
            glBindAttribLocation(program, 2, "aTangent");
            glBindAttribLocation(program, 3, "aBiTangent");
            glBindAttribLocation(program, 4, "aColor");
            glBindAttribLocation(program, 5, "aTexCoord");
            glBindAttribLocation(program, 6, "aWeights");
            glBindAttribLocation(program, 7, "aBoneIds");

            glLinkProgram(program);
            int linkStatus = glGetProgrami(program, GL_LINK_STATUS);

            for (int shader : shaders) {
                glDetachShader(program, shader);
                glDeleteShader(shader); //TODO change based on comment at top of class.
            }

            if (linkStatus == GL_TRUE) {
                glValidateProgram(program);
            } else {
                Log.e("Program link error:\n" +
                        glGetProgramInfoLog(program));
                glDeleteProgram(program);
                program = 0;
            }
        }
        return program;
    }
}
