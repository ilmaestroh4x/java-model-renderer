package ca.hex.engine.test_new.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public final class BufferUtils {
    private BufferUtils() { }

    public static final int BYTES_PER_INT = Integer.SIZE / Byte.SIZE;
    public static final int BYTES_PER_FLOAT = Float.SIZE / Byte.SIZE;

    public static ByteBuffer getByteBuffer(int size) {
        return ByteBuffer.allocateDirect(size).order(ByteOrder.nativeOrder());
    }

    public static ByteBuffer arrayToBuffer(byte[] data) {
        return getByteBuffer(data.length).put(data).flip();
    }

    public static IntBuffer getIntBuffer(int size) {
        return getByteBuffer(size * BYTES_PER_INT).asIntBuffer();
    }

    public static IntBuffer arrayToBuffer(int[] data) {
        return getIntBuffer(data.length).put(data).flip();
    }

    public static FloatBuffer getFloatBuffer(int size) {
        return getByteBuffer(size * BYTES_PER_FLOAT).asFloatBuffer();
    }

    public static FloatBuffer arrayToBuffer(float[] data) {
        return getFloatBuffer(data.length).put(data).flip();
    }
}
