package ca.hex.engine.test_new.model;

import ca.hex.engine.test_new.Log;

import static ca.hex.f_math.Mat.*;
import static ca.hex.f_math.Quat.slerp;
import static ca.hex.f_math.Vec3.lerp;

public class Animation {

    public static class Key {

        private final float mTime;
        private final float[] mValue;

        public Key(float time, float[] data) {
            mTime = time;
            mValue = data;
        }
    }

    public static class Channel {

        private static final float[] TEMP_FLOAT4 = new float[4];

        private final String mName;
        private final Node mNode;
        private final Key[] mPositinKeys;
        private final Key[] mRotationKeys;
        private final Key[] mScaleKeys;

        private final float[] mMatrix = new float[16];

        public Channel(
                String name,
                Node node,
                Key[] positinKeys,
                Key[] rotationKeys,
                Key[] scaleKeys
        ) {
            if (node == null) {
                Log.d("Could not find node: " + name);
            }

            mName = name;
            mNode = node;
            mPositinKeys = positinKeys;
            mRotationKeys = rotationKeys;
            mScaleKeys = scaleKeys;
        }

        private static class LerpData {

            private int mPrevIdx;
            private int mNextIdx;
            private float mDelta;

            private void calculate(Key[] keys, float time) {
                mPrevIdx = 0;
                mNextIdx = 0;
                mDelta   = 1;

                for (int i = 0; i < keys.length; i++) {
                    if (keys[i].mTime <= time) {
                        mPrevIdx = i ;
                    } else {
                        mNextIdx = i;
                        break;
                    }
                }

                if (mPrevIdx > mNextIdx) {
                    //TODO, look into useing the animation duration + key times to calculate a better loop delta.
                    /*
                        i0 = 9, i1 = 0
                        //check amount of time between key[i1].mTime and duration,
                        //generate delta using a virtual "i1 = 10" (10 actually being the mValue of 0 but the mTime of animation duration)
                     */
                    int temp = mPrevIdx;
                    mPrevIdx = mNextIdx;
                    mNextIdx = temp;
                }

                float t0 = keys[mPrevIdx].mTime;
                float t1 = keys[mNextIdx].mTime;

                if (mPrevIdx != mNextIdx) {
                    mDelta = (time - t0) / (t1 - t0);
                }
            }
        }

        private static final LerpData LERP_DATA = new LerpData();

        public void calcTransform(float time) {
            if (mNode == null) return;

            setIdentityM(mMatrix);

            LERP_DATA.calculate(mPositinKeys, time);
            translateM(mMatrix,
                    lerp(TEMP_FLOAT4,
                            mPositinKeys[LERP_DATA.mPrevIdx].mValue,
                            mPositinKeys[LERP_DATA.mNextIdx].mValue,
                            LERP_DATA.mDelta));


            LERP_DATA.calculate(mRotationKeys, time);
            rotateMQ(mMatrix,
                    slerp(TEMP_FLOAT4,
                            mRotationKeys[LERP_DATA.mPrevIdx].mValue,
                            mRotationKeys[LERP_DATA.mNextIdx].mValue,
                            LERP_DATA.mDelta, true));


            LERP_DATA.calculate(mScaleKeys, time);
            scaleM(mMatrix,
                    lerp(TEMP_FLOAT4,
                            mScaleKeys[LERP_DATA.mPrevIdx].mValue,
                            mScaleKeys[LERP_DATA.mNextIdx].mValue,
                            LERP_DATA.mDelta));

            mNode.setTransform(mMatrix);
        }

    }

    private final String mName;
    private final float mDuration;
    private final float mTicksPerSecond;
    private final Channel[] mChannels;

    public Animation(String name, float duration, float ticksPerSecond, Channel[] channels) {
        Log.d("Name: " + name + ", duration: " + duration);

        mName = name;
        mDuration = duration;
        mTicksPerSecond = ticksPerSecond;
        mChannels = channels;
    }

    public void animate(float time) {
        time = (time * mTicksPerSecond) % mDuration;

        for (Channel channel : mChannels) {
            channel.calcTransform(time);
        }
    }
}
