package ca.hex.engine.test_new.model;

import ca.hex.engine.test_new.Shader;
import ca.hex.engine.test_new.gl.Texture2D;

import static ca.hex.f_math.Mat.invertM;
import static ca.hex.f_math.Mat.multiplyMM;
import static ca.hex.f_math.Mat.setIdentityM;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;

public class Material {
    //TODO look into saving shader program state and restoring it for faster "binding"

    private static final float[] TEMP_BONE_TRANSFORM = new float[16];

    private String mName = null;

    private float[] mModelLocalMatrix = new float[16];

    private float[] mModelMatrix = new float[16];
    private float[] mInvModelMatrix = new float[16];

    private float[] mModelViewMatrix = new float[16];

    private float[] mModelViewProjectionMatrix = new float[16];


    private float mOpacity               = 0;
    private float mShininess             = 0;
    private float mShininessStrength     = 0;
    private float mReflectivity          = 0;
    private float mRefractionIndex       = 0;

    private float[] mDiffuseColor        = new float[] { 0, 0, 0, 0 };
    private float[] mAmbientColor        = new float[] { 0, 0, 0, 0 };
    private float[] mSpecularColor       = new float[] { 0, 0, 0, 0 };
    private float[] mEmissiveColor       = new float[] { 0, 0, 0, 0 };
    private float[] mTransparentColor    = new float[] { 0, 0, 0, 0 };
    private float[] mReflectiveColor     = new float[] { 0, 0, 0, 0 };

    private Texture2D mDiffuseTexture    = null;
    private Texture2D mSpecularTexture   = null;
    private Texture2D mAmbientTexture    = null;
    private Texture2D mEmissiveTexture   = null;
    private Texture2D mNormalTexture     = null;
    private Texture2D mShininessTexture  = null;
    private Texture2D mOpacityTexture    = null;

    public Material setName(String name) {
        mName = name != null ? name : "Unnamed_Material";
        return this;
    }

    public Material setOpacity(float opacity) {
        mOpacity = opacity >= 0 ? opacity : 1;
        return this;
    }

    public Material setShininess(float shininess) {
        mShininess = shininess;
        return this;
    }

    public Material setShininessStrength(float shininessStrength) {
        mShininessStrength = shininessStrength;
        return this;
    }

    public Material setReflectivity(float reflectivity) {
        mReflectivity = reflectivity;
        return this;
    }

    public Material setRefractionIndex(float refractionIndex) {
        mRefractionIndex = refractionIndex;
        return this;
    }

    public Material setDiffuseColor(float[] diffuseColor) {
        mDiffuseColor = diffuseColor;
        return this;
    }

    public Material setAmbientColor(float[] ambientColor) {
        mAmbientColor = ambientColor;
        return this;
    }

    public Material setSpecularColor(float[] specularColor) {
        mSpecularColor = specularColor;
        return this;
    }

    public Material setEmissiveColor(float[] emissiveColor) {
        mEmissiveColor = emissiveColor;
        return this;
    }

    public Material setTransparentColor(float[] transparentColor) {
        mTransparentColor = transparentColor;
        return this;
    }

    public Material setReflectiveColor(float[] reflectiveColor) {
        mReflectiveColor = reflectiveColor;
        return this;
    }

    public void setDiffuseTexture(Texture2D diffuseTexture) {
        mDiffuseTexture = diffuseTexture;
    }

    public void setSpecularTexture(Texture2D specularTexture) {
        mSpecularTexture = specularTexture;
    }

    public void setAmbientTexture(Texture2D ambientTexture) {
        mAmbientTexture = ambientTexture;
    }

    public void setEmissiveTexture(Texture2D emissiveTexture) {
        mEmissiveTexture = emissiveTexture;
    }

    public void setNormalTexture(Texture2D normalTexture) {
        mNormalTexture = normalTexture;
    }

    public void setShininessTexture(Texture2D shininessTexture) {
        mShininessTexture = shininessTexture;
    }

    public void setOpacityTexture(Texture2D opacityTexture) {
        mOpacityTexture = opacityTexture;
    }

    public Material() {
        setName(null);
        setOpacity(-1);
    }

    public boolean bind(float[] viewProjection, Shader shader, Bone[] bones, Node node, float[] transform) {
        glUseProgram(shader.getShaderProgram());

        if (node != null) {
            node.getTransform(mModelLocalMatrix);
        } else {
            setIdentityM(mModelLocalMatrix);
        }


        multiplyMM(mModelMatrix, transform, mModelLocalMatrix);
        multiplyMM(mModelViewProjectionMatrix, viewProjection, mModelMatrix);

        invertM(mInvModelMatrix, mModelMatrix);

        //glUniform3fv(shader.getEyePositionLocation(), viewProjection.getPosition());

        glUniformMatrix4fv(shader.getModelLocation(), false, mModelMatrix);
        glUniformMatrix4fv(shader.getInvModelLocation(), false, mInvModelMatrix);

        glUniformMatrix4fv(shader.getModelViewProjectionLocation(), false, mModelViewProjectionMatrix);

        glUniformMatrix4fv(shader.getInvModelLocation(), false, mInvModelMatrix);

        glUniform1f(shader.getOpacityLocation(),            mOpacity);
        glUniform1f(shader.getShininessLocation(),          mShininess);
        glUniform1f(shader.getShininessStrengthLocation(),  mShininessStrength);
        glUniform1f(shader.getRefractionIndexLocation(),    mRefractionIndex);
        glUniform1f(shader.getReflectivityLocation(),       mReflectivity);

        glUniform4fv(shader.getDiffuseColorLocation(),      mDiffuseColor);
        glUniform4fv(shader.getAmbientColorLocation(),      mAmbientColor);
        glUniform4fv(shader.getSpecularColorLocation(),     mSpecularColor);
        glUniform4fv(shader.getEmissiveColorLocation(),     mEmissiveColor);
        glUniform4fv(shader.getTransparentColorLocation(),  mTransparentColor);
        glUniform4fv(shader.getReflectiveColorLocation(),   mReflectiveColor);

        glActiveTexture(GL_TEXTURE0);
        if (mDiffuseTexture != null) {
            glUniform1i(shader.getDiffuseTextureLocation(), 0);
            glBindTexture(mDiffuseTexture.getType(), mDiffuseTexture.getId());
        } else {
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        glActiveTexture(GL_TEXTURE1);
        if (mSpecularTexture != null) {
            glUniform1i(shader.getSpecularTextureLocation(), 1);
            glBindTexture(mSpecularTexture.getType(), mSpecularTexture.getId());
        } else {
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        glActiveTexture(GL_TEXTURE2);
        if (mAmbientTexture != null) {
            glUniform1i(shader.getAmbientTextureLocation(), 2);
            glBindTexture(mAmbientTexture.getType(), mAmbientTexture.getId());
        } else {
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        glActiveTexture(GL_TEXTURE3);
        if (mEmissiveTexture != null) {
            glUniform1i(shader.getEmissiveTextureLocation(), 3);
            glBindTexture(mEmissiveTexture.getType(), mEmissiveTexture.getId());
        } else {
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        glActiveTexture(GL_TEXTURE4);
        if (mNormalTexture != null) {
            glUniform1i(shader.getNormalTextureLocation(), 4);
            glBindTexture(mNormalTexture.getType(), mNormalTexture.getId());
        } else {
            glBindTexture(GL_TEXTURE_2D, 0);
        }


        glActiveTexture(GL_TEXTURE5);
        if (mShininessTexture != null) {
            glUniform1i(shader.getShininessTextureLocation(), 5);
            glBindTexture(mShininessTexture.getType(), mShininessTexture.getId());
        } else {
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        glActiveTexture(GL_TEXTURE6);
        if (mOpacityTexture != null) {
            glUniform1i(shader.getOpacityTextureLocation(), 6);
            glBindTexture(mOpacityTexture.getType(), mOpacityTexture.getId());
        } else {
            glBindTexture(GL_TEXTURE_2D, 0);
        }

/*
        glActiveTexture(GL_TEXTURE2);
        if (mEnvCubemap != null) {
            glUniform1i(shader.getEnvironmentCubeLocation(), 2);
            glBindTexture(mEnvCubemap.getType(), mEnvCubemap.getId());
        } else {
            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        }
*/

        if (bones != null &&
            bones.length > 0) {

            glUniform1i(shader.getHasBonesLocation(), 1 /*true*/);
            for (Bone bone : bones) {
                glUniformMatrix4fv(shader.getBoneLocation(bone.getId()), false, bone.getTransform(TEMP_BONE_TRANSFORM));
            }
        } else {
            glUniform1i(shader.getHasBonesLocation(), 0 /*false*/);
        }

        return mOpacity < 0.99f;
    }

    public void destroy() {
        if (mDiffuseTexture != null) {
            mDiffuseTexture.destroy();
        }
        if (mNormalTexture != null) {
            mNormalTexture.destroy();
        }
    }
}
