package ca.hex.engine.test_new.model;

import static ca.hex.f_math.Mat.multiplyMM;
import static ca.hex.f_math.Mat.setIdentityM;
import static java.lang.System.arraycopy;

public class Bone {

    //TODO Check if nodes transform has changed and cache.

    private static final float[] TEMP_MAT4 = new float[16];

    private final int mId;
    private final String mName;
    private final Node mNode;
    private final float[] mOffsetMatrix;

    private boolean mHasChanged = true;
    private float[] mCachedTransform = new float[16];

    public int getId() {
        return mId;
    }

    public void hasChanged() {
        mHasChanged = true;
    }

    public float[] getTransform(float[] result) {
        if (mHasChanged) {
            mHasChanged = false;

            if (mNode == null) {
                arraycopy(mOffsetMatrix, 0, mCachedTransform, 0, 16);
            } else {
                multiplyMM(mCachedTransform,
                        mNode.getTransform(TEMP_MAT4),
                        mOffsetMatrix);
            }
        }
        arraycopy(mCachedTransform, 0, result, 0, 16);
        return result;
    }

    public Bone(
            int id,
            String name,
            Node node,
            float[] offsetMatrix
    ) {
        mId = id;
        mName = name;
        mNode = node;
        mOffsetMatrix = offsetMatrix;
    }
}
