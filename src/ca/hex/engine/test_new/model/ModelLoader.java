package ca.hex.engine.test_new.model;

import ca.hex.engine.test_new.gl.Texture2D;
import ca.hex.engine.test_new.Log;
import org.lwjgl.PointerBuffer;
import org.lwjgl.assimp.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static ca.hex.engine.FileUtils.fileNameFromPathNoExt;
import static ca.hex.engine.FileUtils.findFileByName;
import static ca.hex.engine.test_new.utils.BufferUtils.getFloatBuffer;
import static ca.hex.engine.test_new.utils.BufferUtils.getIntBuffer;
import static org.lwjgl.assimp.Assimp.*;

public class ModelLoader {
    //TODO get all texcoords / vert colors.

    private static final int DEFAULT_FLAGS =
            aiProcess_FindInvalidData |
            aiProcess_ImproveCacheLocality |
            aiProcess_FlipUVs |
            aiProcess_Triangulate |
            aiProcess_JoinIdenticalVertices |
            aiProcess_OptimizeMeshes |
            aiProcess_OptimizeGraph |
            aiProcess_GenSmoothNormals |
            aiProcess_CalcTangentSpace |
            aiProcess_RemoveRedundantMaterials |
            aiProcess_LimitBoneWeights;

    public static Model load(String path) {
        Log.d("Loading model: " + path);

        AIScene aiScene = aiImportFile(path, DEFAULT_FLAGS);
        chkSceneErr(aiScene);

        AINode aiRootNode = aiScene.mRootNode();
        if (aiRootNode == null) return null;

        Node rootNode = processNodes(aiRootNode, null);

        Material[] materials = processMaterials(aiScene, path);

        Animation[] animations = processAnimations(aiScene, rootNode);
        Mesh[] meshes = processMeshes(aiScene, rootNode, materials);

        Log.d(rootNode.toString());

        return new Model(rootNode, meshes, animations);
    }

    private static void chkSceneErr(AIScene aiScene) {
        if (aiScene == null || aiScene.mRootNode() == null ||
                (aiScene.mFlags() & AI_SCENE_FLAGS_INCOMPLETE) != 0) {
            throw new RuntimeException("Error, " + aiGetErrorString());
        }
    }

    private static Node processNodes(AINode aiNode, Node parent) {
        int numMeshes = aiNode.mNumMeshes();
        int[] meshes = new int[numMeshes];

        IntBuffer meshIndices = aiNode.mMeshes();
        if (meshIndices != null) {
            for (int i = 0; i < numMeshes; i++) {
                meshes[i] = meshIndices.get(i);
            }
        }

        int numChildren = aiNode.mNumChildren();
        Node[] children = new Node[numChildren];

        Node result = new Node(parent, children,
                aiNode.mName().dataString(),
                meshes,
                aiMat4ToFloats(aiNode.mTransformation()));

        PointerBuffer childrenAddresses = aiNode.mChildren();
        if (childrenAddresses != null) {
            for (int i = 0; i < numChildren; i++) {
                AINode childNode = AINode.create(childrenAddresses.get(i));
                children[i] = processNodes(childNode, result);
            }
        }

        return result;
    }

    private static Material[] processMaterials(AIScene aiScene, String path) {
        int numMaterials = aiScene.mNumMaterials();
        Material[] materials = new Material[numMaterials];
        //Log.d("aiScene.mNumMaterials() = " + numMaterials);

        AIString aiPathStr = AIString.create();
        AIColor4D aiColor = AIColor4D.create();
        AIString aiString = AIString.create();

        PointerBuffer materialAddresses = aiScene.mMaterials();
        if (materialAddresses != null) {
            for (int i = 0; i < numMaterials; i++) {
                AIMaterial aiMaterial = AIMaterial.create(materialAddresses.get(i));

                Material builder = new Material();

                int[] max = new int[] { 1 };
                float[] val = new float[1];

                if (aiGetMaterialString(aiMaterial, AI_MATKEY_NAME, aiTextureType_NONE, 0, aiString) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_NAME + " = " + aiString.dataString());
                    builder.setName(aiString.dataString());
                }

                if (aiGetMaterialFloatArray(aiMaterial, AI_MATKEY_OPACITY, aiTextureType_NONE, 0, val, max) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_OPACITY + " = " + val[0]);
                    builder.setOpacity(val[0]);
                }
                if (aiGetMaterialFloatArray(aiMaterial, AI_MATKEY_SHININESS, aiTextureType_NONE, 0, val, max) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_SHININESS + " = " + val[0]);
                    builder.setShininess(val[0]);
                }
                if (aiGetMaterialFloatArray(aiMaterial, AI_MATKEY_SHININESS_STRENGTH, aiTextureType_NONE, 0, val, max) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_SHININESS_STRENGTH + " = " + val[0]);
                    builder.setShininessStrength(val[0]);
                }
                if (aiGetMaterialFloatArray(aiMaterial, AI_MATKEY_REFLECTIVITY, aiTextureType_NONE, 0, val, max) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_REFLECTIVITY + " = " + val[0]);
                    builder.setReflectivity(val[0]);
                }
                if (aiGetMaterialFloatArray(aiMaterial, AI_MATKEY_REFRACTI, aiTextureType_NONE, 0, val, max) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_REFRACTI + " = " + val[0]);
                    builder.setRefractionIndex(val[0]);
                }

                if (aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_DIFFUSE, aiTextureType_NONE, 0, aiColor) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_COLOR_DIFFUSE + " = " + Arrays.toString(aiColor4DToFloats(aiColor)));
                    builder.setDiffuseColor(aiColor4DToFloats(aiColor));
                }
                if (aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_AMBIENT, aiTextureType_NONE, 0, aiColor) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_COLOR_AMBIENT + " = " + Arrays.toString(aiColor4DToFloats(aiColor)));
                    builder.setAmbientColor(aiColor4DToFloats(aiColor));
                }
                if (aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_SPECULAR, aiTextureType_NONE, 0, aiColor) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_COLOR_SPECULAR + " = " + Arrays.toString(aiColor4DToFloats(aiColor)));
                    builder.setSpecularColor(aiColor4DToFloats(aiColor));
                }
                if (aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_EMISSIVE, aiTextureType_NONE, 0, aiColor) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_COLOR_EMISSIVE + " = " + Arrays.toString(aiColor4DToFloats(aiColor)));
                    builder.setEmissiveColor(aiColor4DToFloats(aiColor));
                }
                if (aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_TRANSPARENT, aiTextureType_NONE, 0, aiColor) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_COLOR_TRANSPARENT + " = " + Arrays.toString(aiColor4DToFloats(aiColor)));
                    builder.setTransparentColor(aiColor4DToFloats(aiColor));
                }
                if (aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_REFLECTIVE, aiTextureType_NONE, 0, aiColor) == aiReturn_SUCCESS) {
                    //Log.d(AI_MATKEY_COLOR_REFLECTIVE + " = " + Arrays.toString(aiColor4DToFloats(aiColor)));
                    builder.setReflectiveColor(aiColor4DToFloats(aiColor));
                }

                //TODO get rest of texture into, ie uv index, flags, etc.
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_DIFFUSE, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_DIFFUSE = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        builder.setDiffuseTexture(new Texture2D(textureFilePath, true));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_SPECULAR, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_SPECULAR = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        builder.setSpecularTexture(new Texture2D(textureFilePath));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_AMBIENT, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_AMBIENT = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        builder.setAmbientTexture(new Texture2D(textureFilePath, true));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_EMISSIVE, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_EMISSIVE = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        builder.setEmissiveTexture(new Texture2D(textureFilePath, true));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_HEIGHT, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_HEIGHT = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        Log.d("--TODO--");
                        //builder.setHeightTexture(new Texture2D(textureFilePath));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_NORMALS, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_NORMALS = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        builder.setNormalTexture(new Texture2D(textureFilePath));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_SHININESS, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_SHININESS = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        builder.setShininessTexture(new Texture2D(textureFilePath));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_OPACITY, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_OPACITY = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        builder.setOpacityTexture(new Texture2D(textureFilePath));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_DISPLACEMENT, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_DISPLACEMENT = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        Log.d("--TODO--");
                        //builder.setDisplacmentTexture(new Texture2D(textureFilePath));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_LIGHTMAP, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_LIGHTMAP = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        Log.d("--TODO--");
                        //builder.setLightmapTexture(new Texture2D(textureFilePath));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_REFLECTION, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_REFLECTION = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        Log.d("--TODO--");
                        //builder.setReflectionTexture(new Texture2D(textureFilePath));
                    }
                }
                if (aiGetMaterialTexture(aiMaterial, aiTextureType_UNKNOWN, 0, aiPathStr, (int[]) null, null, null, null, null, null) == aiReturn_SUCCESS) {
                    String textureName = fileNameFromPathNoExt(aiPathStr.dataString());
                    Log.d("aiTextureType_UNKNOWN = " + textureName);

                    String textureFilePath = findFileByName(path, textureName);
                    if (textureFilePath != null) {
                        Log.d("--TODO--");
                        //builder.setUnknownTexture(new Texture2D(textureFilePath));
                    }
                }

                materials[i] = builder;
            }
        }

        return materials;
    }

    private static Animation[] processAnimations(AIScene aiScene, Node rootNode) {
        int numAnimations = aiScene.mNumAnimations();

        Animation[] animations = new Animation[numAnimations];

        PointerBuffer animationAddresses = aiScene.mAnimations();
        if (animationAddresses != null) {
            for (int i = 0; i < numAnimations; i++) {
                AIAnimation aiAnimation = AIAnimation.create(animationAddresses.get(i));

                int numChannels = aiAnimation.mNumChannels();
                Animation.Channel[] channels = new Animation.Channel[numChannels];

                PointerBuffer channelAddresses = aiAnimation.mChannels();
                for (int j = 0; j < numChannels; j++) {
                    AINodeAnim aiNodeAnim = AINodeAnim.create(channelAddresses.get(j));

                    int numPositionKeys = aiNodeAnim.mNumPositionKeys();
                    int numRotationKeys = aiNodeAnim.mNumRotationKeys();
                    int numScalingKeys  = aiNodeAnim.mNumScalingKeys();

                    Animation.Key[] positionKeys = new Animation.Key[numPositionKeys];
                    Animation.Key[] rotationKeys = new Animation.Key[numRotationKeys];
                    Animation.Key[] scaleKeys    = new Animation.Key[numScalingKeys];

                    AIVectorKey.Buffer aiPositionKeys = aiNodeAnim.mPositionKeys();
                    AIQuatKey.Buffer aiRotationKeys   = aiNodeAnim.mRotationKeys();
                    AIVectorKey.Buffer aiScalingKeys  = aiNodeAnim.mScalingKeys();

                    if (aiPositionKeys != null) {
                        for (int k = 0; k < numPositionKeys; k++) {
                            AIVectorKey aiPositionKey = aiPositionKeys.get(k);
                            positionKeys[k] = new Animation.Key(
                                    (float) aiPositionKey.mTime(),
                                    aiVec3ToFloats(aiPositionKey.mValue())
                            );
                        }
                    }

                    if (aiRotationKeys != null) {
                        for (int k = 0; k < numRotationKeys; k++) {
                            AIQuatKey aiRotationKey = aiRotationKeys.get(k);
                            rotationKeys[k] = new Animation.Key(
                                    (float) aiRotationKey.mTime(),
                                    aiQuatToFloats(aiRotationKey.mValue())
                            );
                        }
                    }

                    if (aiScalingKeys != null) {
                        for (int k = 0; k < numScalingKeys; k++) {
                            AIVectorKey aiScaleKey = aiScalingKeys.get(k);
                            scaleKeys[k] = new Animation.Key(
                                    (float) aiScaleKey.mTime(),
                                    aiVec3ToFloats(aiScaleKey.mValue())
                            );
                        }
                    }

                    String channelName = aiNodeAnim.mNodeName().dataString();
                    Node channelNode = rootNode.findByName(channelName);


                    channels[j] = new Animation.Channel(
                            channelName,
                            channelNode,
                            positionKeys,
                            rotationKeys,
                            scaleKeys
                    );

                    if (channelNode != null) {
                        channelNode.setAnimationChannel(channels[j]);
                    }
                }

                animations[i] = new Animation(
                        aiAnimation.mName().dataString(),
                        (float) aiAnimation.mDuration(),
                        (float) aiAnimation.mTicksPerSecond(),
                        channels
                );
            }
        }

        return animations;
    }

    private static Mesh[] processMeshes(AIScene aiScene, Node rootNode, Material[] materials) {
        final int WEIGHTS_PER_VERTEX = 4;
        final int VERTICES_PER_FACE = 3;

        int numMeshes = aiScene.mNumMeshes();
        Mesh[] meshes = new Mesh[numMeshes];

        PointerBuffer meshAddresses = aiScene.mMeshes();
        if (meshAddresses != null) {
            for (int i = 0; i < numMeshes; i++) {
                AIMesh aiMesh = AIMesh.create(meshAddresses.get(i));

                int numBones = aiMesh.mNumBones();
                Bone[] bones = new Bone[numBones];

                int numVertices = aiMesh.mNumVertices();
                int numFaces = aiMesh.mNumFaces();

                FloatBuffer vertexBuffer = getFloatBuffer(numVertices * 3);

                AIVector3D.Buffer aiVertices = aiMesh.mVertices();
                for (int j = 0; j < numVertices; j++) {
                    AIVector3D aiVertex = aiVertices.get(j);
                    vertexBuffer.put(aiVertex.x());
                    vertexBuffer.put(aiVertex.y());
                    vertexBuffer.put(aiVertex.z());
                }

                FloatBuffer normalBuffer = null;

                AIVector3D.Buffer aiNormals = aiMesh.mNormals();
                if (aiNormals != null) {
                    normalBuffer = getFloatBuffer(numVertices * 3);

                    for (int j = 0; j < numVertices; j++) {
                        AIVector3D aiNormal = aiNormals.get(j);
                        normalBuffer.put(aiNormal.x());
                        normalBuffer.put(aiNormal.y());
                        normalBuffer.put(aiNormal.z());
                    }
                }

                FloatBuffer tangentBuffer = null;

                AIVector3D.Buffer aiTangents = aiMesh.mTangents();
                if (aiTangents != null) {
                    tangentBuffer = getFloatBuffer(numVertices * 3);

                    for (int j = 0; j < numVertices; j++) {
                        AIVector3D aiTangent = aiTangents.get(j);
                        tangentBuffer.put(aiTangent.x());
                        tangentBuffer.put(aiTangent.y());
                        tangentBuffer.put(aiTangent.z());
                    }
                }

                FloatBuffer bitangentBuffer = null;

                AIVector3D.Buffer aiBitangents = aiMesh.mBitangents();
                if (aiBitangents != null) {
                    bitangentBuffer = getFloatBuffer(numVertices * 3);

                    for (int j = 0; j < numVertices; j++) {
                        AIVector3D aiBitangent = aiBitangents.get(j);
                        bitangentBuffer.put(aiBitangent.x());
                        bitangentBuffer.put(aiBitangent.y());
                        bitangentBuffer.put(aiBitangent.z());
                    }
                }

                FloatBuffer colorBuffer = null;

                AIColor4D.Buffer aiColors0 = aiMesh.mColors(0);
                if (aiColors0 != null) {
                    colorBuffer = getFloatBuffer(numVertices * 4);

                    for (int j = 0; j < numVertices; j++) {
                        AIColor4D aiColor0 = aiColors0.get(j);
                        colorBuffer.put(aiColor0.r());
                        colorBuffer.put(aiColor0.g());
                        colorBuffer.put(aiColor0.b());
                        colorBuffer.put(aiColor0.a());
                    }
                }

                FloatBuffer texcoordBuffer = null;

                AIVector3D.Buffer aiTexcoords0 = aiMesh.mTextureCoords(0);
                if (aiTexcoords0 != null) {
                    texcoordBuffer = getFloatBuffer(numVertices * 2);

                    for (int j = 0; j < numVertices; j++) {
                        AIVector3D aiTexcoord0 = aiTexcoords0.get(j);
                        texcoordBuffer.put(aiTexcoord0.x());
                        texcoordBuffer.put(aiTexcoord0.y());
                    }
                }


                FloatBuffer boneWeightsBuffer = null;
                IntBuffer boneIdBuffer = null;

                PointerBuffer boneAddresses = aiMesh.mBones();
                if (boneAddresses != null) {
                    boneWeightsBuffer = getFloatBuffer(numVertices * WEIGHTS_PER_VERTEX);
                    boneIdBuffer = getIntBuffer(numVertices * WEIGHTS_PER_VERTEX);

                    for (int j = 0; j < numBones; j++) {
                        AIBone aiBone = AIBone.create(boneAddresses.get(j));

                        int numWeights = aiBone.mNumWeights();

                        AIVertexWeight.Buffer aiVertexWeights = aiBone.mWeights();
                        for (int k = 0; k < numWeights; k++) {
                            AIVertexWeight aiVertexWeight = aiVertexWeights.get(k);

                            int vertexId = aiVertexWeight.mVertexId();
                            int vertexOffset = vertexId * WEIGHTS_PER_VERTEX;

                            float weight = aiVertexWeight.mWeight();

                            for (int l = 0; l < WEIGHTS_PER_VERTEX; l++) {
                                int index = vertexOffset + l;

                                if (boneWeightsBuffer.get(index) == 0) {
                                    boneWeightsBuffer.put(index, weight);
                                    boneIdBuffer.put(index, j);
                                    break;
                                }
                            }
                        }
                        String boneName = aiBone.mName().dataString();
                        Node boneNode = rootNode.findByName(boneName);

                        bones[j] = new Bone(j,
                                boneName,
                                boneNode,
                                aiMat4ToFloats(aiBone.mOffsetMatrix()));

                        boneNode.addBone(bones[j]);
                    }
                }
                IntBuffer indexBuffer = getIntBuffer(numFaces * VERTICES_PER_FACE);

                AIFace.Buffer aiFaces = aiMesh.mFaces();
                for (int j = 0; j < numFaces; j++) {
                    AIFace aiFace = aiFaces.get(j);

                    int numIndices = aiFace.mNumIndices();

                    IntBuffer faceIndices = aiFace.mIndices();
                    for (int k = 0; k < numIndices; k++) {
                        int index = faceIndices.get(k);

                        indexBuffer.put(index);
                    }
                }

                vertexBuffer.position(0);

                if (normalBuffer        != null) normalBuffer.position(0);
                if (tangentBuffer       != null) tangentBuffer.position(0);
                if (bitangentBuffer     != null) bitangentBuffer.position(0);
                if (colorBuffer         != null) colorBuffer.position(0);
                if (texcoordBuffer      != null) texcoordBuffer.position(0);
                if (boneWeightsBuffer   != null) boneWeightsBuffer.position(0);
                if (boneIdBuffer        != null) boneIdBuffer.position(0);

                indexBuffer.position(0);

                Node node;
                if (bones.length == 0) {
                    node = rootNode.findByMeshId(i);
                } else {
                    //When a mesh has a bone, it uses the bones "offset matrix" to calculate its position relative to the scenes root node
                    node = rootNode;
                }

                meshes[i] = new Mesh.Builder()
                        .setName(aiMesh.mName().dataString())
                        .setNode(node)
                        .setMaterial(materials[aiMesh.mMaterialIndex()])
                        .setVertices(vertexBuffer)
                        .setNormals(normalBuffer)
                        .setTangents(tangentBuffer)
                        .setBitangents(bitangentBuffer)
                        .setColors(colorBuffer)
                        .setTexcoords(texcoordBuffer)
                        .setIndices(indexBuffer, numFaces * 3)
                        .setBones(bones, boneWeightsBuffer, boneIdBuffer)
                        .build();
            }
        }

        return meshes;
    }


    private static float[] aiColor4DToFloats(AIColor4D aiColor4D) {
        return new float[] {
                aiColor4D.r(),
                aiColor4D.g(),
                aiColor4D.b(),
                aiColor4D.a()
        };
    }

    private static float[] aiVec3ToFloats(AIVector3D aiVector3D) {
        return new float[] {
                aiVector3D.x(),
                aiVector3D.y(),
                aiVector3D.z()
        };
    }

    private static float[] aiQuatToFloats(AIQuaternion aiQuaternion) {
        return new float[] {
                aiQuaternion.x(),
                aiQuaternion.y(),
                aiQuaternion.z(),
                aiQuaternion.w()
        };
    }

    private static float[] aiMat4ToFloats(AIMatrix4x4 aiMatrix4x4) {
        return new float[]{
                aiMatrix4x4.a1(),
                aiMatrix4x4.b1(),
                aiMatrix4x4.c1(),
                aiMatrix4x4.d1(),

                aiMatrix4x4.a2(),
                aiMatrix4x4.b2(),
                aiMatrix4x4.c2(),
                aiMatrix4x4.d2(),

                aiMatrix4x4.a3(),
                aiMatrix4x4.b3(),
                aiMatrix4x4.c3(),
                aiMatrix4x4.d3(),

                aiMatrix4x4.a4(),
                aiMatrix4x4.b4(),
                aiMatrix4x4.c4(),
                aiMatrix4x4.d4(),
        };
    }
}






































