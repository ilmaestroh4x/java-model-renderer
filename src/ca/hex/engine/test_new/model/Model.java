package ca.hex.engine.test_new.model;

import ca.hex.engine.test_new.Shader;

import static ca.hex.f_math.Mat.*;
import static ca.hex.f_math.Quat.fromAxisAngle;
import static ca.hex.f_math.Quat.identity;
import static ca.hex.f_math.Vec3.one;
import static ca.hex.f_math.Vec3.zero;

public class Model {

    private final Node mRootNode;
    private final Mesh[] mMeshes;
    private final Animation[] mAnimations;

    private final float[] mPosition = zero(new float[3]);
    private final float[] mRotation = identity(new float[4]);
    private final float[] mScale    = one(new float[3]);
    private final float[] mTransform = new float[16];

    public void setPosition(float x, float y, float z) {
        mHasChanged = true;
        mPosition[0] = x;
        mPosition[1] = y;
        mPosition[2] = z;
    }

    public void setRotation(float x, float y, float z, float angle) {
        mHasChanged = true;
        fromAxisAngle(mRotation, new float[] { x, y, z, angle }, true);
    }

    public void setScale(float scale) {
        mHasChanged = true;
        mScale[0] = scale;
        mScale[1] = scale;
        mScale[2] = scale;
    }

    public Model(
            Node rootNode,
            Mesh[] meshes,
            Animation[] animations
    ) {
        mRootNode = rootNode;
        mMeshes = meshes;
        mAnimations = animations;

        setIdentityM(mTransform);
    }

    public void animate(int animationId, float time) {
        if (animationId < mAnimations.length) {
            mAnimations[animationId].animate(time);
        }
    }

    public void render(float[] viewProjection, Shader shader) {
        calcTransform();
        for (Mesh mesh : mMeshes) {
            mesh.render(viewProjection, shader, mTransform);
        }
        //mRootNode.debugDraw();
    }

    public void destroy() {
        for (Mesh mesh : mMeshes) {
            mesh.destroy();
        }
    }

    private boolean mHasChanged = true;
    private void calcTransform() {
        if (mHasChanged) {
            mHasChanged = false;
            setIdentityM(mTransform);
            translateM(mTransform, mPosition);
            rotateMQ(mTransform, mRotation);
            scaleM(mTransform, mScale);
        }
    }
}
