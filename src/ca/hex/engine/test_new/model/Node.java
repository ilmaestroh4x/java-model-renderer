package ca.hex.engine.test_new.model;

import ca.hex.f_math.Mat;

import java.util.HashSet;

import static ca.hex.engine.test_new.gl.GLDebug.drawLine;
import static ca.hex.engine.test_new.gl.GLDebug.drawMatrix4;
import static ca.hex.f_math.Mat.multiplyMM;
import static ca.hex.f_math.Mat.multiplyMV;
import static ca.hex.f_math.Mat.setIdentityM;
import static java.lang.System.arraycopy;

public final class Node {

    //TODO look into more efficient tree caching methods.

    private static final float[] TEMP_MAT4 = new float[16];

    private final Node mParent;
    private final Node[] mChildren;

    private final String mName;
    private final int[] mMeshes;
    private final float[] mTransformMatrix;

    //mBones holds a link to each bone associated with this node (1 per mesh)
    private HashSet<Bone> mBones = new HashSet<>();

    private Animation.Channel mAnimationChannel = null;

    private boolean mHasChanged = true;
    private float[] mCachedTransform = new float[16];

    public void addBone(Bone bone) {
        mBones.add(bone);
    }

    public void setAnimationChannel(Animation.Channel animationChannel) {
        mAnimationChannel = animationChannel;
    }

    private void hasChanged() {
        if (!mHasChanged) {
            mHasChanged = true;
            //Invalidate all linked bones
            mBones.forEach(Bone::hasChanged);

            //Invalidate all child cached transforms.
            for (Node child : mChildren) {
                child.hasChanged();
            }
        }
    }

    public void setTransform(float[] transform) {
        if (!Mat.equals(transform, mTransformMatrix)) {
            arraycopy(transform, 0, mTransformMatrix, 0, 16);
            hasChanged();
        }
    }

    public float[] getTransform(float[] result) {
        if (mHasChanged) {
            mHasChanged = false;

            if (mParent == null) {
                arraycopy(mTransformMatrix, 0, mCachedTransform, 0, 16);
            } else {
                multiplyMM(mCachedTransform,
                        mParent.getTransform(TEMP_MAT4),
                        mTransformMatrix);
            }
        }
        arraycopy(mCachedTransform, 0, result, 0, 16);
        return result;
    }

    public Node(
            Node parent,
            Node[] children,
            String name,
            int[] meshes,
            float[] transformMatrix
    ) {
        mParent = parent;
        mChildren = children;

        mName = name;
        mMeshes = meshes;
        mTransformMatrix = transformMatrix;
    }

    public Node findRoot() {
        if (mParent == null) {
            return this;
        } else {
            return mParent.findRoot();
        }
    }

    public Node findByName(String name) {
        if (mName.equals(name)) {
            return this;
        } else {
            for (Node child : mChildren) {
                Node result = child.findByName(name);
                if (result != null) {
                    return result;
                }
            }
            return null;
        }
    }

    public Node findByMeshId(int meshId) {
        for (int mesh : mMeshes) {
            if (mesh == meshId) {
                return this;
            }
        }
        for (Node child : mChildren) {
            Node result = child.findByMeshId(meshId);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    public float[] getPosition() {
        float[] out = {0, 0, 0, 1};
        multiplyMV(out, getTransform(TEMP_MAT4), out);
        return out;
    }

    public void debugDraw() {
        drawMatrix4(getTransform(TEMP_MAT4));
        if (mParent != null) {
            drawLine(mParent.getPosition(), getPosition());
        }
        for (Node child : mChildren) {
            child.debugDraw();
        }
    }

    @Override
    public String toString() {
        return "\n" + print("", true);
    }

    private String print(final String prefix, final boolean isTail) {
        final StringBuilder result = new StringBuilder(prefix);

        final String next;
        if (isTail) {
            result.append("└───");
            next = prefix + "    ";
        } else {
            result.append("├───");
            next = prefix + "│    ";
        }

        result.append(mName).append(" - ");
        if (mMeshes.length > 0) {
            result.append("[Mesh(es): ").append(mMeshes.length).append("]");
        }
        if (!mBones.isEmpty()) {
            result.append("[Bone(s): ").append(mBones.size()).append("]");
        }
        if (mAnimationChannel != null) {
            result.append("[Animated]");
        }
        result.append("\n");

        for (int i = 0; i < mChildren.length; i++) {
            Node child = mChildren[i];
            boolean isLastChild = (i == mChildren.length - 1);
            result.append(child.print(next, isLastChild));
        }

        return result.toString();
    }
}
