package ca.hex.engine.test_new.model;

import ca.hex.engine.test_new.Shader;
import org.lwjgl.opengl.GL15;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.*;

public class Mesh {

    public static class Builder {

        private String mName = null;
        private Node mNode = null;
        private Bone[] mBones = null;
        private Material mMaterial = null;
        private FloatBuffer mVertexBuffer = null;
        private FloatBuffer mNormalBuffer = null;
        private FloatBuffer mTangentBuffer = null;
        private FloatBuffer mBitangentBuffer = null;
        private FloatBuffer mColorBuffer = null;
        private FloatBuffer mTexcoordBuffer = null;
        private FloatBuffer mBoneWeightsBuffer = null;
        private IntBuffer mBoneIdBuffer = null;
        private IntBuffer mIndexBuffer = null;
        private int mNumIndices = 0;
        private int mMode = 0;

        public Builder setName(String name) {
            mName = name != null ? name : "Unnamed_Mesh";
            return this;
        }

        public Builder setNode(Node node) {
            mNode = node;
            return this;
        }

        public Builder setBones(Bone[] bones, FloatBuffer boneWeightsBuffer, IntBuffer boneIdBuffer) {
            mBones = bones != null ? bones : new Bone[0];
            mBoneWeightsBuffer = boneWeightsBuffer;
            mBoneIdBuffer = boneIdBuffer;
            return this;
        }

        public Builder setMaterial(Material material) {
            mMaterial = material;
            return this;
        }

        public Builder setMode(int mode) {
            mMode = mode >= 0 ? mode : GL_TRIANGLES;
            return this;
        }

        public Builder setVertices(FloatBuffer vertexBuffer) {
            mVertexBuffer = vertexBuffer;
            return this;
        }

        public Builder setNormals(FloatBuffer normalBuffer) {
            mNormalBuffer = normalBuffer;
            return this;
        }

        public Builder setTangents(FloatBuffer tangentBuffer) {
            mTangentBuffer = tangentBuffer;
            return this;
        }

        public Builder setBitangents(FloatBuffer bitangentBuffer) {
            mBitangentBuffer = bitangentBuffer;
            return this;
        }

        public Builder setColors(FloatBuffer colorBuffer) {
            mColorBuffer = colorBuffer;
            return this;
        }

        public Builder setTexcoords(FloatBuffer texcoordBuffer) {
            mTexcoordBuffer = texcoordBuffer;
            return this;
        }

        public Builder setIndices(IntBuffer indexBuffer, int numIndices) {
            mIndexBuffer = indexBuffer;
            mNumIndices = numIndices;
            return this;
        }

        public Builder() {
            setName(null);
            setBones(null, null, null);
            setMode(-1);
        }

        public Mesh build() {
            return new Mesh(this);
        }
    }

    private final String mName;
    private final Node mNode;
    private final Bone[] mBones;
    private final Material mMaterial;
    private final int mNumIndices;
    private final int mMode;

    private final int mVAO;
    private final ArrayList<Integer> mVBOs = new ArrayList<>();

    private Mesh(Builder builder) {
        mName = builder.mName;
        mNode = builder.mNode;
        mBones = builder.mBones;
        mMaterial = builder.mMaterial;
        mNumIndices = builder.mNumIndices;
        mMode = builder.mMode;

        mVAO = glGenVertexArrays();
        glBindVertexArray(mVAO);

        if (builder.mIndexBuffer        != null) mVBOs.add(createIndexVBO(builder.mIndexBuffer));
        if (builder.mVertexBuffer       != null) mVBOs.add(createAttribVBO(0, 3, builder.mVertexBuffer));
        if (builder.mNormalBuffer       != null) mVBOs.add(createAttribVBO(1, 3, builder.mNormalBuffer));
        if (builder.mTangentBuffer      != null) mVBOs.add(createAttribVBO(2, 3, builder.mTangentBuffer));
        if (builder.mBitangentBuffer    != null) mVBOs.add(createAttribVBO(3, 3, builder.mBitangentBuffer));
        if (builder.mColorBuffer        != null) mVBOs.add(createAttribVBO(4, 4, builder.mColorBuffer));
        if (builder.mTexcoordBuffer     != null) mVBOs.add(createAttribVBO(5, 2, builder.mTexcoordBuffer));
        if (builder.mBoneWeightsBuffer  != null) mVBOs.add(createAttribVBO(6, 4, builder.mBoneWeightsBuffer));
        if (builder.mBoneIdBuffer       != null) mVBOs.add(createAttribVBO(7, 4, builder.mBoneIdBuffer));

        glBindVertexArray(0);
    }

    public void render() {
        render(null, null, null);
    }

    public void render(float[] viewProjection, Shader shader, float[] transform) {
        boolean isTransparent = false;

        if (mMaterial != null) {
            isTransparent = mMaterial.bind(viewProjection, shader, mBones, mNode, transform);
        }

        if (!isTransparent) {
            glBindVertexArray(mVAO);
            //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mVBOs.get(0));//0 holds the index buffer.
            glDrawElements(mMode, mNumIndices, GL_UNSIGNED_INT, 0);
            //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
        }
    }

    public void destroy() {
        glDeleteVertexArrays(mVAO);
        mVBOs.forEach(GL15::glDeleteBuffers);

        if (mMaterial != null) {
            mMaterial.destroy();
        }
    }

    private static int createAttribVBO(int index, int size, FloatBuffer floatBuffer) {
        int id = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, id);
        glBufferData(GL_ARRAY_BUFFER, floatBuffer, GL_STATIC_DRAW);
        glVertexAttribPointer(index, size, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(index);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return id;
    }

    private static int createAttribVBO(int index, int size, IntBuffer intBuffer) {
        int id = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, id);
        glBufferData(GL_ARRAY_BUFFER, intBuffer, GL_STATIC_DRAW);
        glVertexAttribIPointer(index, size, GL_INT, 0, 0);
        glEnableVertexAttribArray(index);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return id;
    }

    private static int createIndexVBO(IntBuffer intBuffer) {
        int id = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, intBuffer, GL_STATIC_DRAW);
        //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        return id;
    }

}
