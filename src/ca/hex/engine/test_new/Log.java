package ca.hex.engine.test_new;

public final class Log {
    private Log() { }

    public static void d(String message) {
        StackTraceElement stackTrace = Thread.currentThread().getStackTrace()[2];
        System.out.println(stackTrace.getClassName() + "." + stackTrace.getMethodName() + ":L" + stackTrace.getLineNumber() + " : " + message);
    }

    public static void e(String message) {
        StackTraceElement stackTrace = Thread.currentThread().getStackTrace()[2];
        System.out.println(stackTrace.getClassName() + "." + stackTrace.getMethodName() + ":L" + stackTrace.getLineNumber() + " : " + message);
    }
}
