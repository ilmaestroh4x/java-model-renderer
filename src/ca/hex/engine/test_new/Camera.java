package ca.hex.engine.test_new;

import ca.hex.f_math.*;

import static org.lwjgl.glfw.GLFW.*;

public class Camera {

    //TODO optimize (currently recalculating the view matrix each frame (even if nothing has changed))

    private static final int X = 0;
    private static final int Y = 1;
    private static final int Z = 2;
    private static final int ANGLE = 3;

    private boolean mHasChanged = true;

    private final float[] mPosition = { 0, 0, 0 };

    private final float[] mViewMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];

    private final float[] mViewProjectionMatrix = new float[16];

    private float mFoV = 65.0f;
    private float mNearClipPlane = 0.1f;
    private float mFarClipPlane  = 1000.0f;

    public float[] getPosition() {
        return mPosition;
    }

    public float[] getViewMatrix() {
        return mViewMatrix;
    }

    public float[] getProjectionMatrix() {
        return mProjectionMatrix;
    }

    public float[] getViewProjectionMatrix() {
        if (mHasChanged) {
            mHasChanged = false;

            Mat.multiplyMM(mViewProjectionMatrix, mProjectionMatrix, mViewMatrix);
        }
        return mViewProjectionMatrix;
    }

    public Camera() {
        Mat.setIdentityM(mViewMatrix);
        Mat.setIdentityM(mProjectionMatrix);
        Mat.setIdentityM(mViewProjectionMatrix);
    }

    public void onResize(int width, int height) {
        float aspect = (float) width / height;
        Mat.perspectiveM(mProjectionMatrix, mFoV, aspect, mNearClipPlane, mFarClipPlane);

        mHasChanged = true;
    }

    private float mLookSpeed = 0.15f;

    private final float[] mRotDelta = { 0, 0 };
    private final float[] mRotMagnitude = { 0 };

    private final float[] mRotAccumulator = { 0, 0 };

    private final float[] mRotAxisAngleUp = { 0, 1, 0, 0 };
    private final float[] mRotAxisAngleRt = { 1, 0, 0, 0 };

    private final float[] mRotX = { 0, 0, 0, 0 };
    private final float[] mRotY = { 0, 0, 0, 0 };

    private final float[] mRotXY = { 0, 0, 0, 0 };

    private final float[] mRotInitial = { 0, 0, 0, 1 };
    private final float[] mRotCurrent = { 0, 0, 0, 1 };

    private final float[] mRotLocalFw = { 0, 0, 0 };
    private final float[] mRotLocalUp = { 0, 0, 0 };
    private final float[] mRotLocalRt = { 0, 0, 0 };

    private final float[] mLookTarget = { 0, 0, 0 };

    public void onMouseInput(float deltaX, float deltaY) {
        mRotDelta[0] = deltaX;
        mRotDelta[1] = deltaY;

        Vec2.mag(mRotMagnitude, mRotDelta);
        Vec2.div(mRotDelta, mRotDelta, mRotMagnitude[0]);
        Vec2.mul(mRotDelta, mRotDelta, mRotMagnitude[0] * mLookSpeed);
        Vec2.add(mRotAccumulator, mRotAccumulator, mRotDelta);

        mRotAccumulator[X] = mRotAccumulator[X] % 360f;
        mRotAccumulator[Y] = Utils.clamp(mRotAccumulator[Y], -90, 90);

        mRotAxisAngleUp[ANGLE] = mRotAccumulator[X];
        mRotAxisAngleRt[ANGLE] = mRotAccumulator[Y];

        Quat.fromAxisAngle(mRotX, mRotAxisAngleUp, true);
        Quat.fromAxisAngle(mRotY, mRotAxisAngleRt, true);

        Quat.mulQ(mRotXY, mRotX, mRotY);
        Quat.mulQ(mRotCurrent, mRotXY, mRotInitial);

        Quat.localForward(mRotLocalFw, mRotCurrent);
        Quat.localUp(mRotLocalUp, mRotCurrent);
        Quat.localRight(mRotLocalRt, mRotCurrent);
    }

    private float mWalkSpeed = 5.0f;
    private float mSprintSpeed = 25.0f;

    private final float[] mMoveValue = { 0 };
    private final float[] mMoveSpeed = { 0 };

    private final float[] mInputVector = { 0, 0, 0 };
    private final float[] mInputMagnitude = { 0 };

    private final float[] mMoveVector = { 0, 0, 0 };

    private final float[] mMoveForward = { 0, 0, 0 };
    private final float[] mMoveStrafe = { 0, 0, 0 };

    public void onKeyInput(int key, int action) {
        if (action != GLFW_PRESS && action != GLFW_RELEASE) return;

        mMoveValue[0] = action == GLFW_RELEASE ? -1 : 1;

        boolean inputVectorChanged = true;
        switch (key) {
            default: inputVectorChanged = false;
            case GLFW_KEY_LEFT_SHIFT: {
                mMoveSpeed[0] = action == GLFW_RELEASE ? mWalkSpeed : mSprintSpeed;
            } break;
            case GLFW_KEY_W: {
                mInputVector[Z] -= mMoveValue[0];
            } break;
            case GLFW_KEY_A: {
                mInputVector[X] -= mMoveValue[0];
            } break;
            case GLFW_KEY_S: {
                mInputVector[Z] += mMoveValue[0];
            } break;
            case GLFW_KEY_D: {
                mInputVector[X] += mMoveValue[0];
            } break;
        }

        if (inputVectorChanged) {
            Vec3.mag(mInputMagnitude, mInputVector);
        }
    }

    public void onUpdate(float timeStep) {
        if (mInputMagnitude[0] > 0) {
            Vec3.div(mMoveVector, mInputVector, mInputMagnitude[0]);
            Vec3.mul(mMoveVector, mMoveVector, mMoveSpeed[0] * timeStep);

            Vec3.mul(mMoveForward, mRotLocalFw, mMoveVector[Z]);
            Vec3.mul(mMoveStrafe,  mRotLocalRt, mMoveVector[X]);

            Vec3.add(mPosition, mPosition, mMoveForward);
            Vec3.add(mPosition, mPosition, mMoveStrafe);
        }
    }

    public void onRender(float alpha) {
        Vec3.sub(mLookTarget, mPosition, mRotLocalFw);
        Mat.setLookAtM(mViewMatrix, mPosition, mLookTarget, mRotLocalUp);

        mHasChanged = true;
    }
}
