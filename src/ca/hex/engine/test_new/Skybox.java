package ca.hex.engine.test_new;

import ca.hex.engine.test_new.gl.Cubemap;
import ca.hex.engine.test_new.model.Mesh;
import ca.hex.engine.test_new.utils.ShaderUtils;
import ca.hex.engine.FileUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static ca.hex.engine.test_new.utils.BufferUtils.getFloatBuffer;
import static ca.hex.engine.test_new.utils.BufferUtils.getIntBuffer;
import static ca.hex.f_math.Mat.invertM;
import static ca.hex.f_math.Mat.setIdentityM;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL20.*;

public class Skybox {

    private static final Mesh MESH;
    static {
        FloatBuffer vertexBuffer = getFloatBuffer(4 * 3);
        IntBuffer indexBuffer = getIntBuffer(4);
        vertexBuffer.put(-1);
        vertexBuffer.put(-1);
        vertexBuffer.put( 1);
        indexBuffer.put(0);

        vertexBuffer.put( 1);
        vertexBuffer.put(-1);
        vertexBuffer.put( 1);
        indexBuffer.put(1);

        vertexBuffer.put( 1);
        vertexBuffer.put( 1);
        vertexBuffer.put( 1);
        indexBuffer.put(2);

        vertexBuffer.put(-1);
        vertexBuffer.put( 1);
        vertexBuffer.put( 1);
        indexBuffer.put(3);

        vertexBuffer.flip();
        indexBuffer.flip();

        MESH = new Mesh.Builder()
                .setVertices(vertexBuffer)
                .setIndices(indexBuffer, 4)
                .setMode(GL_TRIANGLE_FAN)
                .build();
    }

    private static final int SHADER;
    private static final int SHADER_INV_MODEL_VIEW;
    private static final int SHADER_INV_PROJ;
    static {
        String vs = FileUtils.readFileToString("src/res/shaders/skybox/skybox.vert");
        String fs = FileUtils.readFileToString("src/res/shaders/skybox/skybox.frag");

        SHADER = ShaderUtils.load(vs, fs);

        SHADER_INV_MODEL_VIEW = glGetUniformLocation(SHADER, "uInvModelView");
        SHADER_INV_PROJ = glGetUniformLocation(SHADER, "uInvProj");
        glUseProgram(SHADER);
        glUniform1i(glGetUniformLocation(SHADER, "uSkycube"), 0);
        glUseProgram(0);
    }

    private static final float[] TEMP_MATRIX = new float[16];

    private Cubemap mCubemap;

    public Skybox(Cubemap cubemap) {
        mCubemap = cubemap;
    }

    public void render(Camera camera) {
        glDepthFunc(GL_LEQUAL);
        glUseProgram(SHADER);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, mCubemap.getId());

        invertM(TEMP_MATRIX, camera.getProjectionMatrix());
        glUniformMatrix4fv(SHADER_INV_PROJ, false, TEMP_MATRIX);
        glUniformMatrix4fv(SHADER_INV_MODEL_VIEW, true, camera.getViewMatrix());
        MESH.render();

        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

        glUseProgram(0);
        glDepthFunc(GL_LESS);
    }

    public void destroy() {
        mCubemap.destroy();
    }
}
