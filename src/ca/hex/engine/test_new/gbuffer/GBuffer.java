package ca.hex.engine.test_new.gbuffer;

import ca.hex.engine.FileUtils;
import ca.hex.engine.test_new.Shader;
import ca.hex.engine.test_new.gl.Framebuffer;
import ca.hex.engine.test_new.gl.RenderBuffer;
import ca.hex.engine.test_new.gl.Texture2D;
import ca.hex.engine.test_new.utils.ShaderUtils;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL14.GL_DEPTH_COMPONENT32;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL30.GL_COLOR_ATTACHMENT3;
import static org.lwjgl.opengl.GL30.GL_DEPTH_ATTACHMENT;

public final class GBuffer {

    private Shader mShader;
    private Framebuffer mGBuffer = null;

    public int getId() {
        return mGBuffer.getId();
    }

    public Shader getShader() {
        return mShader;
    }

    private Integer mPositionAttachment = null;
    public int getPositionAttachment() {
        if (mPositionAttachment == null) {
            mPositionAttachment = mGBuffer.getAttachment(GL_COLOR_ATTACHMENT0).getId();
        }
        return mPositionAttachment;
    }

    private Integer mNormalAttachment = null;
    public int getNormalAttachment() {
        if (mNormalAttachment == null) {
            mNormalAttachment = mGBuffer.getAttachment(GL_COLOR_ATTACHMENT1).getId();
        }
        return mNormalAttachment;
    }

    private Integer mColorRoughnessAttachment = null;
    public int getColorRoughnessAttachment() {
        if (mColorRoughnessAttachment == null) {
            mColorRoughnessAttachment = mGBuffer.getAttachment(GL_COLOR_ATTACHMENT2).getId();
        }
        return mColorRoughnessAttachment;
    }

    private Integer mEmissionMetallicAttachment = null;
    public int getEmissionMetallicAttachment() {
        if (mEmissionMetallicAttachment == null) {
            mEmissionMetallicAttachment = mGBuffer.getAttachment(GL_COLOR_ATTACHMENT3).getId();
        }
        return mEmissionMetallicAttachment;
    }

    public GBuffer() {
        mShader = new Shader(
                ShaderUtils.load(
                        FileUtils.readFileToString("src/res/shaders/gbuffer/gbuffer.vert"),
                        FileUtils.readFileToString("src/res/shaders/gbuffer/gbuffer.frag")
                )
        );
    }

    public void onResize(int width, int height) {
        if (mGBuffer != null) {
            mGBuffer.destroy();
            mGBuffer = null;

            mPositionAttachment = null;
            mNormalAttachment = null;
            mColorRoughnessAttachment = null;
            mEmissionMetallicAttachment = null;
        }

        mGBuffer = new Framebuffer(
                new Framebuffer.Attachment(GL_COLOR_ATTACHMENT0, new Texture2D(width, height, GL_RGB32F, GL_RGB, GL_FLOAT)),        //Position
                new Framebuffer.Attachment(GL_COLOR_ATTACHMENT1, new Texture2D(width, height, GL_RGB32F, GL_RGB, GL_FLOAT)),        //Normal
                new Framebuffer.Attachment(GL_COLOR_ATTACHMENT2, new Texture2D(width, height, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE)), //Albedo + Roughness
                new Framebuffer.Attachment(GL_COLOR_ATTACHMENT3, new Texture2D(width, height, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE)), //Emission + Metallic
                new Framebuffer.Attachment(GL_DEPTH_ATTACHMENT, new RenderBuffer(width, height, GL_DEPTH_COMPONENT32))      //Depth Test (NO SAMPLING)
        );
    }

    public void destroy() {
        mShader.destroy();
        mGBuffer.destroy();
    }
}
