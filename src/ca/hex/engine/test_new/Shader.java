package ca.hex.engine.test_new;

import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;

public class Shader {

    private final int mShaderProgram;

    private final int mEyePositionLocation;

    private final int mModelLocation;
    private final int mViewLocation;
    private final int mProjectionLocation;
    private final int mModelViewProjectionLocation;

    private final int mInvModelLocation;
    private final int mInvViewLocation;
    private final int mInvProjectionLocation;

    private final int mOpacityLocation;
    private final int mShininessLocation;
    private final int mShininessStrengthLocation;
    private final int mRefractionIndexLocation;
    private final int mReflectivityLocation;

    private final int mDiffuseColorLocation;
    private final int mAmbientColorLocation;
    private final int mSpecularColorLocation;
    private final int mEmissiveColorLocation;
    private final int mTransparentColorLocation;
    private final int mReflectiveColorLocation;

    private final int mDiffuseTextureLocation;
    private final int mSpecularTextureLocation;
    private final int mAmbientTextureLocation;
    private final int mEmissiveTextureLocation;
    private final int mHeightTextureLocation;
    private final int mNormalTextureLocation;
    private final int mShininessTextureLocation;
    private final int mOpacityTextureLocation;

    private final int mEnvironmentCubeLocation;

    private final int mHasBonesLocation;
    private final int[] mBoneLocations = new int[100];

    public int getShaderProgram() {
        return mShaderProgram;
    }


    public int getEyePositionLocation() {
        return mEyePositionLocation;
    }


    public int getModelLocation() {
        return mModelLocation;
    }

    public int getViewLocation() {
        return mViewLocation;
    }

    public int getProjectionLocation() {
        return mProjectionLocation;
    }

    public int getModelViewProjectionLocation() {
        return mModelViewProjectionLocation;
    }


    public int getInvModelLocation() {
        return mInvModelLocation;
    }

    public int getInvViewLocation() {
        return mInvViewLocation;
    }

    public int getInvProjectionLocation() {
        return mInvProjectionLocation;
    }


    public int getOpacityLocation() {
        return mOpacityLocation;
    }

    public int getShininessLocation() {
        return mShininessLocation;
    }

    public int getShininessStrengthLocation() {
        return mShininessStrengthLocation;
    }

    public int getRefractionIndexLocation() {
        return mRefractionIndexLocation;
    }

    public int getReflectivityLocation() {
        return mReflectivityLocation;
    }


    public int getDiffuseColorLocation() {
        return mDiffuseColorLocation;
    }

    public int getAmbientColorLocation() {
        return mAmbientColorLocation;
    }

    public int getSpecularColorLocation() {
        return mSpecularColorLocation;
    }

    public int getEmissiveColorLocation() {
        return mEmissiveColorLocation;
    }

    public int getTransparentColorLocation() {
        return mTransparentColorLocation;
    }

    public int getReflectiveColorLocation() {
        return mReflectiveColorLocation;
    }


    public int getDiffuseTextureLocation() {
        return mDiffuseTextureLocation;
    }

    public int getSpecularTextureLocation() {
        return mSpecularTextureLocation;
    }

    public int getAmbientTextureLocation() {
        return mAmbientTextureLocation;
    }

    public int getEmissiveTextureLocation() {
        return mEmissiveTextureLocation;
    }

    public int getHeightTextureLocation() {
        return mHeightTextureLocation;
    }

    public int getNormalTextureLocation() {
        return mNormalTextureLocation;
    }

    public int getShininessTextureLocation() {
        return mShininessTextureLocation;
    }

    public int getOpacityTextureLocation() {
        return mOpacityTextureLocation;
    }

    public int getEnvironmentCubeLocation() {
        return mEnvironmentCubeLocation;
    }


    public int getHasBonesLocation() {
        return mHasBonesLocation;
    }

    public int getBoneLocation(int boneId) {
        if (mBoneLocations.length > boneId) {
            return mBoneLocations[boneId];
        } else {
            return -1;
        }
    }

    public Shader(int shaderProgram) {
        mShaderProgram               = shaderProgram;

        mEyePositionLocation         = glGetUniformLocation(shaderProgram, "uEyePosition");

        mModelLocation               = glGetUniformLocation(shaderProgram, "uModel");
        mViewLocation                = glGetUniformLocation(shaderProgram, "uView");
        mProjectionLocation          = glGetUniformLocation(shaderProgram, "uProjection");
        mModelViewProjectionLocation = glGetUniformLocation(shaderProgram, "uModelViewProjection");

        mInvModelLocation            = glGetUniformLocation(shaderProgram, "uInvModel");
        mInvViewLocation             = glGetUniformLocation(shaderProgram, "uInvView");
        mInvProjectionLocation       = glGetUniformLocation(shaderProgram, "uInvProjection");

        mOpacityLocation             = glGetUniformLocation(shaderProgram, "uOpacity");
        mShininessLocation           = glGetUniformLocation(shaderProgram, "uShininess");
        mShininessStrengthLocation   = glGetUniformLocation(shaderProgram, "uShininessStrength");
        mReflectivityLocation        = glGetUniformLocation(shaderProgram, "uReflectivity");
        mRefractionIndexLocation     = glGetUniformLocation(shaderProgram, "uRefractionIndex");
        mDiffuseColorLocation        = glGetUniformLocation(shaderProgram, "uDiffuseColor");
        mAmbientColorLocation        = glGetUniformLocation(shaderProgram, "uAmbientColor");
        mSpecularColorLocation       = glGetUniformLocation(shaderProgram, "uSpecularColor");
        mEmissiveColorLocation       = glGetUniformLocation(shaderProgram, "uEmissiveColor");
        mTransparentColorLocation    = glGetUniformLocation(shaderProgram, "uTransparentColor");
        mReflectiveColorLocation     = glGetUniformLocation(shaderProgram, "uReflectiveColor");

        mDiffuseTextureLocation      = glGetUniformLocation(shaderProgram, "uDiffuseTexture");
        mSpecularTextureLocation     = glGetUniformLocation(shaderProgram, "uSpecularTexture");
        mAmbientTextureLocation      = glGetUniformLocation(shaderProgram, "uAmbientTexture");
        mEmissiveTextureLocation     = glGetUniformLocation(shaderProgram, "uEmissiveTexture");
        mHeightTextureLocation       = glGetUniformLocation(shaderProgram, "uHeightTexture");
        mNormalTextureLocation       = glGetUniformLocation(shaderProgram, "uNormalTexture");
        mShininessTextureLocation    = glGetUniformLocation(shaderProgram, "uShininessTexture");
        mOpacityTextureLocation      = glGetUniformLocation(shaderProgram, "uOpacityTexture");

        mEnvironmentCubeLocation     = glGetUniformLocation(shaderProgram, "uEnvironmentCubemap");

        mHasBonesLocation            = glGetUniformLocation(shaderProgram, "uHasBones");
        for (int i = 0; i < mBoneLocations.length; i++) {
            mBoneLocations[i]        = glGetUniformLocation(shaderProgram, "uBones[" + i + "]");
        }
    }

    public void destroy() {
        glDeleteProgram(mShaderProgram);
    }
}
