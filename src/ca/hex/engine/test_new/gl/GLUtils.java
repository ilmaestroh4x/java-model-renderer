package ca.hex.engine.test_new.gl;

import ca.hex.engine.test_new.Log;

import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.stb.STBImage.*;

public final class GLUtils {
    private GLUtils() { }

    public static int getFormat(int components) {
        int format;

        switch (components) {
            default: Log.d("Unknown format. " + components + " component(s). (Using GL_RGBA)");
            case 4: format = GL_RGBA;               break;
            case 3: format = GL_RGB;                break;
            case 2: format = GL_LUMINANCE_ALPHA;    break;
            case 1: format = GL_LUMINANCE;          break;
        }

        return format;
    }

    public static boolean isPoT(int number) {
        return number > 0 && ((number & (number - 1)) == 0);
    }

    public static boolean isPoT(int width, int height) {
        return isPoT(width) && isPoT(height);
    }

    public static boolean imageLoad(int offset, int[] width, int[] height, int[] format, ByteBuffer[] data, String path) {
        int[] w = new int[1];
        int[] h = new int[1];
        int[] c = new int[1];

        data[offset] = stbi_load(path, w, h, c, STBI_default);

        if (data[offset] != null) {
            width[offset] = w[0];
            height[offset] = h[0];
            format[offset] = getFormat(c[0]);

            if (!isPoT(w[0], h[0])) {
                Log.d("Discarding image. Reason:Image is not power of two. Path:" + path);

                stbi_image_free(data[offset]);
                data[offset] = null;
            }
        } else {
            Log.d("Failed to load image. Reason:" + stbi_failure_reason() + " Path:" + path);
        }

        return data[offset] != null;
    }

    public static void imageFree(ByteBuffer data) {
        if (data != null) {
            stbi_image_free(data);
        }
    }

    public static void imagesFree(ByteBuffer[] data) {
        for (ByteBuffer aData : data) {
            imageFree(aData);
        }
    }
}
