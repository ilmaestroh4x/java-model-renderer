package ca.hex.engine.test_new.gl;

import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glGenTextures;

public abstract class Texture implements Framebuffer.Attachable {

    private final int mId;

    @Override
    public final int getId() {
        return mId;
    }

    public abstract int getType();

    public Texture() {
        mId = glGenTextures();
    }

    @Override
    public void destroy() {
        glDeleteTextures(mId);
    }
}
