package ca.hex.engine.test_new.gl;

import ca.hex.engine.test_new.Log;

import java.nio.ByteBuffer;

import static ca.hex.engine.test_new.gl.GLUtils.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL12.GL_TEXTURE_WRAP_R;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X;
import static org.lwjgl.opengl.GL21.GL_SRGB;
import static org.lwjgl.opengl.GL21.GL_SRGB_ALPHA;

public class Cubemap extends Texture {

    @Override
    public int getType() {
        return GL_TEXTURE_CUBE_MAP;
    }

    public Cubemap(int width, int height) {
        this(width, height, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE);
    }

    public Cubemap(int width, int height, int innerFormat, int format, int type) {
        if (width != height) {
            glBindTexture(GL_TEXTURE_CUBE_MAP, getId());

            for (int i = 0; i < 6; i++) {
                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, innerFormat, width, height, 0, format, type, 0);
            }

            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        } else {
            Log.d("Discarding Texture. Reason:Width != Height. [w" + width + ", h" + height + "]");
            destroy();
        }
    }

    public Cubemap(String pathRight, String pathLeft,
                   String pathTop, String pathBottom,
                   String pathFront, String pathBack) {
        this(pathRight, pathLeft, pathTop, pathBottom, pathFront, pathBack, false);
    }

    public Cubemap(String pathRight, String pathLeft,
                   String pathTop, String pathBottom,
                   String pathFront, String pathBack,
                   boolean useSRGBInnerFormat) {

        boolean isLoaded = true;

        int[] widths = new int[6];
        int[] heights = new int[6];
        int[] formats = new int[6];
        ByteBuffer[] dataBuffers = new ByteBuffer[6];

        isLoaded &= imageLoad(0, widths, heights, formats, dataBuffers, pathRight);
        isLoaded &= imageLoad(1, widths, heights, formats, dataBuffers, pathLeft);
        isLoaded &= imageLoad(2, widths, heights, formats, dataBuffers, pathTop);
        isLoaded &= imageLoad(3, widths, heights, formats, dataBuffers, pathBottom);
        isLoaded &= imageLoad(4, widths, heights, formats, dataBuffers, pathFront);
        isLoaded &= imageLoad(5, widths, heights, formats, dataBuffers, pathBack);

        if (isLoaded) {
            glBindTexture(GL_TEXTURE_CUBE_MAP, getId());

            for (int i = 0; i < 6; i++) {

                if (useSRGBInnerFormat) {
                    int innerFormat = formats[i];

                    switch (innerFormat) {
                        case GL_RGB: {
                            innerFormat = GL_SRGB;
                        } break;
                        case GL_RGBA: {
                            innerFormat = GL_SRGB_ALPHA;
                        } break;
                    }

                    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, innerFormat, widths[i], heights[i], 0, formats[i], GL_UNSIGNED_BYTE, dataBuffers[i]);
                } else {
                    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, formats[i], widths[i], heights[i], 0, formats[i], GL_UNSIGNED_BYTE, dataBuffers[i]);
                }

                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, formats[i], widths[i], heights[i], 0, formats[i], GL_UNSIGNED_BYTE, dataBuffers[i]);
            }

            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        }

        imagesFree(dataBuffers);
    }
}
