package ca.hex.engine.test_new.gl;

import static org.lwjgl.opengl.GL30.*;

public class RenderBuffer implements Framebuffer.Attachable {

    private final int mId;

    @Override
    public final int getId() {
        return mId;
    }

    @Override
    public int getType() {
        return GL_RENDERBUFFER;
    }

    public RenderBuffer(int width, int height, int format) {
        mId = glGenRenderbuffers();

        glBindRenderbuffer(GL_RENDERBUFFER, mId);

        glRenderbufferStorage(GL_RENDERBUFFER, format, width, height);

        glBindRenderbuffer(GL_RENDERBUFFER, 0);
    }

    @Override
    public void destroy() {
        glDeleteRenderbuffers(mId);
    }
}
