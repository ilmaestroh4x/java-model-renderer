package ca.hex.engine.test_new.gl;

import java.nio.ByteBuffer;

import static ca.hex.engine.test_new.gl.GLUtils.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL21.GL_SRGB;
import static org.lwjgl.opengl.GL21.GL_SRGB_ALPHA;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

public class Texture2D extends Texture {

    @Override
    public int getType() {
        return GL_TEXTURE_2D;
    }

    public Texture2D(int width, int height) {
        this(width, height, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE);
    }

    public Texture2D(int width, int height, int innerFormat, int format, int type) {
        glBindTexture(GL_TEXTURE_2D, getId());

        glTexImage2D(GL_TEXTURE_2D, 0, innerFormat, width, height, 0, format, type, 0);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        //glGenerateMipmap(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public Texture2D(String path) {
        this(path, false);
    }

    public Texture2D(String path, boolean useSRGBInnerFormat) {

        int[] width = new int[1];
        int[] height = new int[1];
        int[] format = new int[1];
        ByteBuffer[] data = new ByteBuffer[1];

        if (imageLoad(0, width, height, format, data, path)) {
            glBindTexture(GL_TEXTURE_2D, getId());

            if (useSRGBInnerFormat) {
                int innerFormat = format[0];

                switch (innerFormat) {
                    case GL_RGB: {
                        innerFormat = GL_SRGB;
                    } break;
                    case GL_RGBA: {
                        innerFormat = GL_SRGB_ALPHA;
                    } break;
                }

                glTexImage2D(GL_TEXTURE_2D, 0, innerFormat, width[0], height[0], 0, format[0], GL_UNSIGNED_BYTE, data[0]);
            } else {
                glTexImage2D(GL_TEXTURE_2D, 0, format[0], width[0], height[0], 0, format[0], GL_UNSIGNED_BYTE, data[0]);
            }


            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

            glGenerateMipmap(GL_TEXTURE_2D);

            glBindTexture(GL_TEXTURE_2D, 0);
        }

        imagesFree(data);
    }
}
