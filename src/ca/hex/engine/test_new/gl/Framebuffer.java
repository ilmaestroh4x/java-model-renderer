package ca.hex.engine.test_new.gl;

import ca.hex.engine.test_new.Log;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import static ca.hex.engine.test_new.utils.BufferUtils.getIntBuffer;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_NONE;
import static org.lwjgl.opengl.GL11.glReadBuffer;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP;
import static org.lwjgl.opengl.GL14.GL_DEPTH_COMPONENT32;
import static org.lwjgl.opengl.GL20.glDrawBuffers;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL30.glFramebufferRenderbuffer;
import static org.lwjgl.opengl.GL32.glFramebufferTexture;

public class Framebuffer {

    public interface Attachable {
        int getId();
        int getType();
        void destroy();
    }

    public final static class Attachment {

        private final int mAttachment;
        private final Attachable mAttachable;

        public final int getAttachment() {
            return mAttachment;
        }

        public final Attachable getAttachable() {
            return mAttachable;
        }

        public final int getId() {
            return mAttachable.getId();
        }

        public final int getTarget() {
            return mAttachable.getType();
        }

        public Attachment(int attachment, Attachable attachable) {
            mAttachment = attachment;
            mAttachable = attachable;
        }
    }

    private final int mId;
    private final HashMap<Integer, Attachable> mAttachments;

    public final int getId() {
        return mId;
    }

    public Attachable getAttachment(int attachment) {
        return mAttachments.get(attachment);
    }

    public Framebuffer(int width, int height) {
        this(
                new Attachment(GL_COLOR_ATTACHMENT0, new Texture2D(width, height)),
                new Attachment(GL_DEPTH_ATTACHMENT, new RenderBuffer(width, height, GL_DEPTH_COMPONENT32))
        );
    }

    public Framebuffer(Attachment... framebufferAttachments) {
        mId = glGenFramebuffers();
        mAttachments = new HashMap<>();

        glBindFramebuffer(GL_FRAMEBUFFER, mId);

        ArrayList<Integer> colorAttachments = new ArrayList<>();

        for (Attachment framebufferAttachment : framebufferAttachments) {
            int attachment = framebufferAttachment.getAttachment();
            int target = framebufferAttachment.getTarget();
            int id = framebufferAttachment.getId();

            mAttachments.put(attachment, framebufferAttachment.getAttachable());

            if (attachment >= GL_COLOR_ATTACHMENT0 &&
                attachment <= GL_COLOR_ATTACHMENT31) {
                colorAttachments.add(attachment);
            }

            switch (target) {
                case GL_TEXTURE_1D: {
                    glFramebufferTexture1D(GL_FRAMEBUFFER, attachment, target, id, 0);
                } break;
                case GL_TEXTURE_2D: {
                    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, target, id, 0);
                } break;
                case GL_TEXTURE_CUBE_MAP: {
                    glFramebufferTexture(GL_FRAMEBUFFER, attachment, id, 0);
                } break;
                case GL_RENDERBUFFER: {
                    glFramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, target, id);
                } break;
                default: {
                    Log.d("Unsupported target type: " + target);
                }
            }
        }

        if (colorAttachments.isEmpty()) {
            glDrawBuffer(GL_NONE);
            glReadBuffer(GL_NONE);
        } else {
            IntBuffer drawBuffers = getIntBuffer(colorAttachments.size());
            for (Integer colorAttachment : colorAttachments) {
                drawBuffers.put(colorAttachment);
            }
            drawBuffers.flip();
            glDrawBuffers(drawBuffers);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        if (checkFramebufferStatus() != GL_FRAMEBUFFER_COMPLETE) {
            destroy();
        }
    }

    public void destroy() {
        mAttachments.forEach((k, v) -> v.destroy());
        glDeleteFramebuffers(mId);
    }

    private static int checkFramebufferStatus() {
        int framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);

        switch (framebufferStatus) {
            case GL_FRAMEBUFFER_COMPLETE: {
                Log.d("framebufferStatus = GL_FRAMEBUFFER_COMPLETE");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: {
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: {
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER: {
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER: {
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER");
            } break;
            case GL_FRAMEBUFFER_UNSUPPORTED: {
                Log.e("framebufferStatus = GL_FRAMEBUFFER_UNSUPPORTED");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE: {
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE");
            } break;
            case GL_FRAMEBUFFER_UNDEFINED: {
                Log.e("framebufferStatus = GL_FRAMEBUFFER_UNDEFINED");
            } break;
        }

        return framebufferStatus;
    }
}
