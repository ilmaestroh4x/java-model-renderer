package ca.hex.engine.test_new.gl;

import java.util.ArrayList;

import static ca.hex.f_math.Mat.multiplyMV;
import static ca.hex.f_math.Vec3.*;
import static org.lwjgl.opengl.GL11.*;

public class GLDebug {

    private static final float[] TMP_FLOAT4_0 = new float[4];
    private static final float[] TMP_FLOAT4_1 = new float[4];

    private static final ArrayList<float[]> LINES = new ArrayList<>();
    private static final ArrayList<float[]> POINTS = new ArrayList<>();

    public static void drawLine(float[] start, float[] end) {
        drawLine(start, end, 1, 1, 1);
    }

    public static void drawLine(float[] start, float[] end, float r, float g, float b) {
        LINES.add(new float[] { start[0], start[1], start[2], r, g, b });
        LINES.add(new float[] { end[0], end[1], end[2], r, g, b });
    }

    public static void drawPoint(float[] point) {
        drawPoint(point, 1, 1, 1);
    }

    public static void drawPoint(float[] point, float r, float g, float b) {
        POINTS.add(new float[] { point[0], point[1], point[2], r, g, b });
    }

    public static void drawMatrix4(float[] matrix4) {
        zero(TMP_FLOAT4_0);
        TMP_FLOAT4_0[3] = 1;

        zero(TMP_FLOAT4_1);
        TMP_FLOAT4_1[3] = 1;

        multiplyMV(TMP_FLOAT4_0, matrix4, TMP_FLOAT4_0);

        forward(TMP_FLOAT4_1);
        multiplyMV(TMP_FLOAT4_1, matrix4, TMP_FLOAT4_1);
        drawLine(TMP_FLOAT4_0, TMP_FLOAT4_1, 0, 0, 1);

        up(TMP_FLOAT4_1);
        multiplyMV(TMP_FLOAT4_1, matrix4, TMP_FLOAT4_1);
        drawLine(TMP_FLOAT4_0, TMP_FLOAT4_1, 0, 1, 0);

        right(TMP_FLOAT4_1);
        multiplyMV(TMP_FLOAT4_1, matrix4, TMP_FLOAT4_1);
        drawLine(TMP_FLOAT4_0, TMP_FLOAT4_1, 1, 0, 0);
    }

    public static void drawDebugging(float[] projectionMatrix, float[] viewMatrix) {
        if (LINES.isEmpty() && POINTS.isEmpty()) return;

        glClear(GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(projectionMatrix);

        glMatrixMode(GL_MODELVIEW);
        glLoadMatrixf(viewMatrix);


        glBegin(GL_LINES);
        for (float[] lp : LINES) {
            glColor4f(lp[3], lp[4], lp[5], 1);
            glVertex3f(lp[0], lp[1], lp[2]);
        }
        glEnd();

        glPointSize(5.0f);

        glBegin(GL_POINTS);
        for (float[] p : POINTS) {
            glColor4f(p[3], p[4], p[5], 1);
            glVertex3f(p[0], p[1], p[2]);
        }
        glEnd();

        LINES.clear();
        POINTS.clear();
    }
}
