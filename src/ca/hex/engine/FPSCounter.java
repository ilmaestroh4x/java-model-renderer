package ca.hex.engine;

public class FPSCounter {

    private int fps = 0;
    private int mTickCount;

    private double mStartTime;
    private double mCurrTime;

    public FPSCounter() {
        start();
    }

    public void start() {
        mTickCount = 0;
        mStartTime = System.currentTimeMillis() * 0.001;
        mCurrTime = mStartTime;
    }

    public int onTick() {
        mTickCount += 1;
        mCurrTime = System.currentTimeMillis() * 0.001;
        if (mStartTime < mCurrTime - 1) {
            mStartTime = mCurrTime;
            fps = mTickCount;
            mTickCount = 0;
        }
        return fps;
    }
}
