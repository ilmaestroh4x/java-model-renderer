package ca.hex.engine;

import ca.hex.engine.test_new.Log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {

    public static String readFileToString(String filePath) {
        return readFileToString(Paths.get(filePath));
    }

    public static String readFileToString(Path filePath) {
        try {
            return new String(Files.readAllBytes(filePath));
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static byte[] readFileToBytes(String filePath) {
        return readFileToBytes(Paths.get(filePath));
    }

    public static byte[] readFileToBytes(Path filePath) {
        try {
            return Files.readAllBytes(filePath);
            //return ByteBuffer.wrap(Files.readAllBytes(filePath));
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static String fileNameFromPath(String path) {
        return new File(path).getName();
    }

    public static String fileNameFromPathNoExt(String path) {
        String name = fileNameFromPath(path);
        int i = name.lastIndexOf('.');
        if (i != -1) {
            return name.substring(0, i);
        } else {
            return name;
        }
    }

    public static String findFileByName(String initialPath, String name) {
        try {
            //TODO find a better way of doing this.
            Path initialPathScope = Paths.get(initialPath).getParent();

            Path result = Files
                    .find(initialPathScope, 3, (path, basicFileAttributes) ->
                            path.toString().toLowerCase()
                            .contains(name.toLowerCase()))
                    .findFirst().orElse(null);

            if (result != null) {
                Log.d("Looking for: " + name + " Found File: " + result.toString());
                return result.toString();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
