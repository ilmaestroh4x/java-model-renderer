package ca.hex.engine.old;

import ca.hex.f_math.Mat;
import ca.hex.f_math.Quat;
import ca.hex.f_math.Vec3;

public class Camera extends Component {
    protected Camera(Entity entity) { super(entity); }

    //TODO add getter and setters to adjust cam on fly
    private float mFoV = 65.0f;
    private float mNearClippingPlane = 0.25f;
    private float mFarClippingPlane = 500.0f;

    private float[] mProjMatrix = new float[16];
    private float[] mViewMatrix = new float[16];

    private Transform mTransform;
    private float[] mPosition = {0, 0, 0};
    private float[] mRotation = {0, 0, 0, 1};
    private float[] mLocalFw  = {0, 0, 0};
    private float[] mLocalUp  = {0, 0, 0};

    @Override
    public void onInitialize() {
        mTransform = entity().transform();
    }

    @Override
    public void onResize(int width, int height) {
        Mat.perspectiveM(mProjMatrix, 0, mFoV, (float) width / height, mNearClippingPlane, mFarClippingPlane);
    }

    @Override
    public void onRender(float alpha) {
        mTransform.getPosition(mPosition);
        mTransform.getRotation(mRotation);

        //TODO This should probably be the transform matrix.
        Quat.localForward(mLocalFw, 0, mRotation, 0);
        Vec3.sub(mLocalFw, 0, mPosition, 0, mLocalFw, 0);
        Quat.localUp(mLocalUp, 0, mRotation, 0);
        Mat.setIdentityM(mViewMatrix, 0);
        Mat.setLookAtM(mViewMatrix, 0, mPosition, mLocalFw, mLocalUp);
    }
}
