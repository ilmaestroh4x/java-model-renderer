package ca.hex.engine.old.texture;

import static org.lwjgl.opengl.GL15.glDeleteBuffers;

public class RenderTextureCubemap extends TextureCubemap {

    private final RenderTarget mRenderTarget;

    public RenderTarget getRenderTarget() {
        return mRenderTarget;
    }

    public RenderTextureCubemap(final int size, final boolean useDepth) {
        super(size);
        mRenderTarget = new RenderTarget(this, useDepth);
    }

    @Override
    public final void destroy() {
        glDeleteBuffers(new int[] {mRenderTarget.getFBOHandle()});
        super.destroy();
    }
}