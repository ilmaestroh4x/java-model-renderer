package ca.hex.engine.old.texture;

import ca.hex.engine.test_new.utils.Image;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;

public class Texture2D extends Texture {

    public Texture2D(int width, int height) {
        super(width, height, 1, GL_TEXTURE_2D);
    }

    public Texture2D(Image image) {
        super(image, GL_TEXTURE_2D);
    }

}
