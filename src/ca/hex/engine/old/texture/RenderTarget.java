package ca.hex.engine.old.texture;

import java.nio.ByteBuffer;

import static ca.hex.engine.test_new.utils.BufferUtils.getByteBuffer;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X;
import static org.lwjgl.opengl.GL14.GL_DEPTH_COMPONENT16;
import static org.lwjgl.opengl.GL30.*;

public class RenderTarget {

    private final Texture mTexture;

    private final boolean mHasDepth;

    private final int mFrameBufferHandle;

    public final boolean hasDepth() {
        return mHasDepth;
    }

    public final int getFBOHandle() {
        return mFrameBufferHandle;
    }

    public RenderTarget(final Texture texture, final boolean hasDepth) {
        mTexture = texture;
        mHasDepth = hasDepth;
        mFrameBufferHandle = createFBO(texture, hasDepth);
    }

    public final void readPixels(final int x, final int y, final int width, final int height, final ByteBuffer pixelBuffer) {
        glBindFramebuffer(GL_FRAMEBUFFER, mFrameBufferHandle);
        glReadPixels(x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixelBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    public final void readPixel(final int x, final int y, final ByteBuffer pixelBuffer){
        readPixels(x, y, 1, 1, pixelBuffer);
    }

    public final void readAllPixels(final ByteBuffer pixelBuffer){
        readPixels(0, 0, mTexture.getWidth(), mTexture.getHeight(), pixelBuffer);
    }

    public final ByteBuffer readPixels(final int x, final int y, final int width, final int height){
        final ByteBuffer pixelBuffer = getByteBuffer(width * height * 4);
        readPixels(x, y, width, height, pixelBuffer);
        return pixelBuffer;
    }

    public final ByteBuffer readPixel(final int x, final int y){
        return readPixels(x, y, 1, 1);
    }

    public final ByteBuffer readAllPixels(){
        return readPixels(0, 0, mTexture.getWidth(), mTexture.getHeight());
    }

    private static int createFBO(final Texture texture, final boolean useDepth) {
        final int[] fboHandle = new int[1];
        glGenFramebuffers(fboHandle);
        glBindFramebuffer(GL_FRAMEBUFFER, fboHandle[0]);

        if (texture instanceof RenderTextureCubemap) {
            for (int i = 0; i < 6; i++) {
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, texture.getTexture(), 0);
            }
        } else if (texture instanceof RenderTexture) {
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.getTexture(), 0);
        }

        if (useDepth) {
            final int[] depthRenderBuffer = new int[1];
            glGenRenderbuffers(depthRenderBuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer[0]);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, texture.getWidth(), texture.getHeight());
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer[0]);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        final int status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            switch (status) {
                case GL_FRAMEBUFFER_UNSUPPORTED:
                    throw new RuntimeException("Error: UNSUPPORTED #" + status + " - Failed to create FBO.");
                default:
                    throw new RuntimeException("Error: UNKNOWN #" + status + " - Failed to create FBO.");
            }
        }

        return fboHandle[0];
    }
}
