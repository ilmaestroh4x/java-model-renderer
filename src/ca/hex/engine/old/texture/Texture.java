package ca.hex.engine.old.texture;

import ca.hex.engine.test_new.utils.Image;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL12.GL_TEXTURE_WRAP_R;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

public abstract class Texture {

    private int[] mTextures;
    private int mTarget;
    private int mWidth;
    private int mHeight;

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getTexture() {
        return mTextures[0];
    }

    public int getTexture(int index) {
        return mTextures[index];
    }

    public int[] getTextures() {
        return mTextures;
    }

    public Texture(int width, int height, int texCount, int target) {
        mTextures = createTexture(mWidth, mHeight, texCount, target);
    }

    public Texture(Image image) {
        this(image, GL_TEXTURE_2D);
    }

    public Texture(Image image, int target) {
        this(new Image[] { image }, target);
    }

    public Texture(Image[] images) {
        this(images, GL_TEXTURE_CUBE_MAP);
    }

    public Texture(Image[] images, int target) {
        mTextures = createTexture(images, target);
        mTarget = target;
        mWidth = images[0].getWidth();
        mHeight = images[0].getHeight();
    }

    private static int[] createTexture(final int width, final int height, final int texCount, final int target) {
        final int[] texHandles;

        if (target == GL_TEXTURE_CUBE_MAP) {
            texHandles = new int[1];
            glGenTextures(texHandles);

            glBindTexture(target, texHandles[0]);

            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

            for (int i = 0; i < 6; i++) {
                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
            }
        } else {
            texHandles = new int[texCount];
            glGenTextures(texHandles);

            for (final int texHandle : texHandles) {
                glBindTexture(target, texHandle);
                glTexImage2D(target, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            }
        }

        glBindTexture(target, 0);

        return texHandles;
    }

    private static int[] createTexture(final Image[] images, final int target) {
        final int[] textures;

        if (target == GL_TEXTURE_CUBE_MAP) {
            textures = new int[1];
            glGenTextures(textures);
            glBindTexture(target, textures[0]);
        } else {
            textures = new int[images.length];
            glGenTextures(textures);
        }

        for (int i = 0; i < images.length; i++) {
            final Image image = images[i];

            int format;

            switch (image.getComponents()) {
                default:
                case 4:
                    format = GL_RGBA;
                    break;
                case 3:
                    format = GL_RGB;
                    break;
                case 2:
                    format = GL_LUMINANCE_ALPHA;
                    break;
                case 1:
                    format = GL_LUMINANCE;
                    break;
            }

            if (target == GL_TEXTURE_CUBE_MAP) {
                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, format, image.getWidth(), image.getHeight(), 0, format, GL_UNSIGNED_BYTE, image.getData());
            } else {
                glBindTexture(target, textures[i]);

                glTexImage2D(GL_TEXTURE_2D, 0, format, image.getWidth(), image.getHeight(), 0, format, GL_UNSIGNED_BYTE, image.getData());

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

                glGenerateMipmap(GL_TEXTURE_2D);
            }
        }

        if (target == GL_TEXTURE_CUBE_MAP) {
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        }

        glBindTexture(target, 0);

        return textures;
    }

    public void bind(int offset) {
        switch (mTarget) {
            default:
            case GL_TEXTURE_2D: {
                for (int i = 0; i < mTextures.length; i++) {
                    glActiveTexture(GL_TEXTURE0 + offset + i);
                    glBindTexture(mTarget, mTextures[i]);
                }
            } break;
            case GL_TEXTURE_CUBE_MAP: {
                glActiveTexture(GL_TEXTURE0 + offset);
                glBindTexture(mTarget, mTextures[0]);
            } break;
        }
    }

    public void unbind() {
        glBindTexture(mTarget, 0);
    }

    public void destroy() {
        glBindTexture(mTarget, 0);
        glDeleteTextures(mTextures);
    }
}