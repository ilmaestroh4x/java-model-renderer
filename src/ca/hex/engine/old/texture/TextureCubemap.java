package ca.hex.engine.old.texture;

import ca.hex.engine.test_new.utils.Image;

import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP;

public class TextureCubemap extends Texture {

    public TextureCubemap(final int size) {
        super(size, size, 6, GL_TEXTURE_CUBE_MAP);
    }

    public TextureCubemap(Image right, Image left,
                          Image top, Image bottom,
                          Image front, Image back) {
        super(new Image[] {
                right, left,
                top, bottom,
                front, back
        }, GL_TEXTURE_CUBE_MAP);
    }
}
