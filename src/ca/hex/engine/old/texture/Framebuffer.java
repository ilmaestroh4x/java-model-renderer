package ca.hex.engine.test_new;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL12.GL_TEXTURE_WRAP_R;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X;
import static org.lwjgl.opengl.GL14.GL_DEPTH_COMPONENT32;
import static org.lwjgl.opengl.GL30.*;

//https://learnopengl.com/Advanced-OpenGL/Framebuffers
public class Framebuffer {

    private final int mWidth;
    private final int mHeight;
    private final int mTexture;
    private final int mFBO;

    public Framebuffer(int width, int height) {
        mWidth = width;
        mHeight = height;

        //create framebuffer
        int[] fbo = new int[1];
        glGenFramebuffers(fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, fbo[0]);

        boolean isRGB = true;
        if (isRGB) {
            //create & attache texture
            int texture = genTexture(mWidth, mHeight);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

            //create & attache depth renderbuffer object (for depth testing, not sampling)
            int renderbuffer = getDepthRenderBuffer(mWidth, mHeight);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderbuffer);

            mTexture = texture;
        } else {
            //create & attache depth texture (for only storing a float depth value, can sample)
            int depthTexture = genDepthTexture(mWidth, mHeight);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);
            glDrawBuffer(GL_NONE);
            glReadBuffer(GL_NONE);

            mTexture = depthTexture;
        }

        checkFramebufferStatus();

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        mFBO = fbo[0];
    }

    public void bind() {
        glViewport(0, 0, mWidth, mHeight);
        glBindFramebuffer(GL_FRAMEBUFFER, mFBO);

    }

    public void destroy() {
        glDeleteFramebuffers(mFBO);
    }

    private static int genTexture(int width, int height) {
        int[] tex = new int[1];

        glGenTextures(tex);
        glBindTexture(GL_TEXTURE_2D, tex[0]);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glBindTexture(GL_TEXTURE_2D, 0);

        return tex[0];
    }

    private static int genDepthTexture(int width, int height) {
        int[] tex = new int[1];

        glGenTextures(tex);
        glBindTexture(GL_TEXTURE_2D, tex[0]);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glBindTexture(GL_TEXTURE_2D, 0);

        return tex[0];
    }

    private static int genCubemap(int width, int height) {
        int[] tex = new int[1];

        glGenTextures(tex);
        glBindTexture(GL_TEXTURE_CUBE_MAP, tex[0]);

        for (int i = 0; i < 6; i++) {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
        }

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

        return tex[0];
    }

    private static int getDepthRenderBuffer(int width, int height) {
        //rbo (Render Buffer Object)
        int[] rbos = new int[1];

        glGenRenderbuffers(rbos);
        glBindRenderbuffer(GL_RENDERBUFFER, rbos[0]);

        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, width, height);

        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        return rbos[0];
    }

    private static int checkFramebufferStatus() {
        int framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        switch (framebufferStatus) {
            case GL_FRAMEBUFFER_COMPLETE:
                Log.d("framebufferStatus = GL_FRAMEBUFFER_COMPLETE");
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT");
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER");
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER");
                break;
            case GL_FRAMEBUFFER_UNSUPPORTED:
                Log.e("framebufferStatus = GL_FRAMEBUFFER_UNSUPPORTED");
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
                Log.e("framebufferStatus = GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE");
                break;
            case GL_FRAMEBUFFER_UNDEFINED:
                Log.e("framebufferStatus = GL_FRAMEBUFFER_UNDEFINED");
                break;
        }
        return framebufferStatus;
    }
}
