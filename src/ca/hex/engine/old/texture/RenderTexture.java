package ca.hex.engine.old.texture;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;

public class RenderTexture extends Texture2D {

    private final RenderTarget mRenderTarget;

    public final RenderTarget getRenderTarget() {
        return mRenderTarget;
    }

    public RenderTexture(final int width, final int height, final boolean useDepth) {
        this(width, height, useDepth, GL_LINEAR, GL_LINEAR);
    }

    public RenderTexture(final int width, final int height, final boolean useDepth, final int filterMode) {
        this(width, height, useDepth, filterMode, filterMode);
    }

    public RenderTexture(final int width, final int height, final boolean useDepth, final int minFilterMode, final int maxFilterMode) {
        super(width, height);
        mRenderTarget = new RenderTarget(this, useDepth);
    }

    @Override
    public final void destroy() {
        glDeleteBuffers(new int[] {mRenderTarget.getFBOHandle()});
        super.destroy();
    }
}