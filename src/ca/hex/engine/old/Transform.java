package ca.hex.engine.old;

import ca.hex.f_math.Mat;
import ca.hex.f_math.Quat;
import ca.hex.f_math.Vec3;

import java.util.ArrayList;

public final class Transform extends Component {
    protected Transform(Entity entity) { super(entity); }

    private static final float[] TMP = new float[] {0, 0, 0, 1};
    private static final float[] UP_VEC = {0, 1, 0};

    private Transform mParent = null;
    private ArrayList<Transform> mChildren = new ArrayList<>();

    public Transform getParent() {
        return mParent;
    }

    public void setParent(Transform transform) {
        if (mParent != null) {
            mParent.mChildren.remove(this);
        }
        mParent = transform;
        if (mParent != null) {
            mParent.mChildren.add(this);
        }
    }

    public void addChild(Transform transform) {
        if (!mChildren.contains(transform)) {
            transform.setParent(this);
        }
    }

    public void removeChild(Transform transform) {
        if (mChildren.contains(transform)) {
            transform.setParent(null);
        }
    }

    @Override
    public void onDestroy() {
        setParent(null);
    }

    private boolean mHasChanged = false;
    private void onChange() {
        mHasChanged = true;
        //Let all children know a change has occurred.
        mChildren.forEach(Transform::onChange);
    }

    private float[] mPosition = new float[] {0, 0, 0};
    private float[] mRotation = new float[] {0, 0, 0, 1};
    private float[] mScale    = new float[] {1, 1, 1};

    private float[] mMatrix = new float[16];

    public final float[] getMatrix() {
        if (mHasChanged) {
            //TODO factor in multiplication of parent matrix
            Mat.setIdentityM(mMatrix);
            Mat.translateM(mMatrix, mPosition);
            Mat.rotateM(mMatrix, mRotation);
            Mat.scaleM(mMatrix, mScale);
            mHasChanged = false;
        }
        return mMatrix;
    }

    public final float[] getPosition(final float[] outVec3) {
        Vec3.copy(outVec3, 0, mPosition, 0);
        return outVec3;
    }

    public final float[] getScale(final float[] outVec3) {
        Vec3.copy(outVec3, 0, mScale, 0);
        return outVec3;
    }

    public final float[] getRotation(final float[] outQuat) {
        Quat.copy(outQuat, 0, mRotation, 0);
        return outQuat;
    }

    public final void setPosition(final float[] inVec3) {
        if (!Vec3.equals(mPosition, 0, inVec3, 0)) {
            Vec3.copy(mPosition, 0, inVec3, 0);
            onChange();
        }
    }

    public final void setScale(final float[] inVec3) {
        if (!Vec3.equals(mScale, 0, inVec3, 0)) {
            Vec3.copy(mScale, 0, inVec3, 0);
            onChange();
        }
    }

    public final void setRotation(final float[] inQuat) {
        if (!Quat.equals(mRotation, 0, inQuat, 0)) {
            Quat.copy(mRotation, 0, inQuat, 0);
            onChange();
        }
    }

/*
    public final void lookAt(final float[] inTargetVec3) {
        lookAt(inTargetVec3, UP_VEC);
    }

    public final void lookAt(final float[] inTargetVec3, final float[] inUpVec3) {
        mHasChanged = true;
        Vec3.sub(TMP, inTargetVec3, mEyePosition);
        Vec3.norm(TMP, TMP);
        Quat.fromLookRotation(mRotation, TMP, inUpVec3);
    }
*/
    public final void transformPoint(final float[] outVec3, final float[] inVec3) {
        Vec3.copy(TMP, 0, inVec3, 0);
        Mat.multiplyMV(TMP, 0, getMatrix(), 0, TMP, 0);
        Vec3.copy(outVec3, 0, TMP, 0);
    }
}
