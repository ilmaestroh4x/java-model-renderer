package ca.hex.engine.old;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class Component {

    public static Component create(Class<? extends Component> componentClass, Entity owner) {
        try {
            Constructor<? extends Component> constructor = componentClass.getDeclaredConstructor(Entity.class);
            constructor.setAccessible(true);
            Component component = constructor.newInstance(owner);
            constructor.setAccessible(false);
            return component;
        } catch (InstantiationException |
                IllegalAccessException |
                InvocationTargetException |
                NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    private final Entity mEntity;

    public Entity entity() {
        return mEntity;
    }

    protected Component(Entity entity) {
        mEntity = entity;
    }

    public void onResize(int width, int height) { }

    public void onKeyInput(int key, int action) { }

    public void onMouseInput(float deltaX, float deltaY) { }

    public void onUpdate(float timeStep) { }

    public void onRender(float alpha) { }

    public void onDestroy() { }

    public void onInitialize() { }
}
