package ca.hex.engine.test_new.utils;


import java.nio.ByteBuffer;

import static org.lwjgl.stb.STBImage.*;

public class Image {

    private final ByteBuffer mData;
    private final int mWidth;
    private final int mHeight;
    private final int mComponents;

    public ByteBuffer getData() {
        return mData;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getComponents() {
        return mComponents;
    }

    public Image(String imagePath) {
        int[] width = new int[1];
        int[] height = new int[1];
        int[] components = new int[1];

        mData = stbi_load(imagePath, width, height, components, STBI_default);
        mWidth = width[0];
        mHeight = height[0];
        mComponents = components[0];
    }
}
