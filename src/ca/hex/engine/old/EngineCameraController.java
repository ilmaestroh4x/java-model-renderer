package ca.hex.engine.old;

import ca.hex.f_math.Quat;
import ca.hex.f_math.Utils;
import ca.hex.f_math.Vec2;
import ca.hex.f_math.Vec3;

import static org.lwjgl.glfw.GLFW.*;

public class EngineCameraController extends Component {
    protected EngineCameraController(Entity entity) { super(entity); }

    private static final float LOOK_SPEED   = 0.15f;

    private float[] rotInitial      = {0, 0, 0, 1};
    private float[] rotCurrent      = {0, 0, 0, 1};
    private float[] rotLocFw        = {0, 0, 0};
    private float[] rotLocRt        = {0, 0, 0};

    private static final float WALK_SPEED   = 5.0f;
    private static final float SPRINT_SPEED = 25.0f;

    private float[] posCurrent      = {0, 0, 0};

    private static final int ROT_DELTA = 0;
    private static final int ROT_DELTA_X = 0;
    private static final int ROT_DELTA_Y = 1;

    private static final int ROT_DELTA_MAG = 2;

    private static final int ROT_ACCUM = 3;
    private static final int ROT_ACCUM_X = 3;
    private static final int ROT_ACCUM_Y = 4;

    private static final int ROT_AXIS_UP = 5;
    private static final int ROT_AXIS_UP_ANGLE = 8;

    private static final int ROT_AXIS_RT = 9;
    private static final int ROT_AXIS_RT_ANGLE = 12;

    private static final int ROT_X = 13;
    private static final int ROT_Y = 17;

    private static final int ROT_XY = 17;

    private float[] rData = {
            0, 0,       //0 - mouse delta
            0,          //2 - delta Magnitude
            0, 0,       //3 - rotCurrent accumulators
            0, 1, 0, 0, //5 - up axis angle
            1, 0, 0, 0, //9 - right axis angle
            0, 0, 0, 0, //13 - rot quat X
            0, 0, 0, 0, //17 - rot quat Y
            0, 0, 0, 0, //21 - rot XY
    };

    private static final int MOV_INPUT_VEC = 0;
    private static final int MOV_INPUT_VEC_X = 0;
    private static final int MOV_INPUT_VEC_Y = 1;
    private static final int MOV_INPUT_VEC_Z = 2;

    private static final int MOV_INPUT_VALUE = 3;

    private static final int MOV_INPUT_MAG = 4;

    private static final int MOV_SPEED = 5;

    private static final int MOV_VECTOR = 6;
    private static final int MOV_VECTOR_X = 6;
    private static final int MOV_VECTOR_Y = 7;
    private static final int MOV_VECTOR_Z = 8;

    private static final int MOV_STRAFE_OFFSET = 9;
    private static final int MOV_FORWARD_OFFSET = 12;

    private float[] mData = {
            0, 0, 0,    //0 - "WASD" input vector
            0,          //3 - input value
            0,          //4 - input magnitude
            WALK_SPEED, //5 - move speed
            0, 0, 0,    //6 - move vec
            0, 0, 0,    //9 - strafe movement
            0, 0, 0,    //12 - forward movement
    };

    private Transform mTransform;

    @Override
    public void onInitialize() {
        mTransform = entity().transform();
    }

    @Override
    public void onMouseInput(float deltaX, float deltaY) {
        rData[ROT_DELTA_X] = deltaX;
        rData[ROT_DELTA_Y] = deltaY;

        Vec2.mag(rData, ROT_DELTA_MAG, rData, ROT_DELTA);
        Vec2.div(rData, ROT_DELTA, rData, ROT_DELTA, rData[ROT_DELTA_MAG]);
        Vec2.mul(rData, ROT_DELTA, rData, ROT_DELTA, rData[ROT_DELTA_MAG] * LOOK_SPEED);

        Vec2.add(rData, ROT_ACCUM, rData, ROT_ACCUM, rData, ROT_DELTA);

        if (rData[ROT_ACCUM_X] <   0) rData[ROT_ACCUM_X] += 360;
        if (rData[ROT_ACCUM_X] > 360) rData[ROT_ACCUM_X] -= 360;
        rData[ROT_ACCUM_Y] = Utils.clamp(rData[ROT_ACCUM_Y], -90, 90);

        rData[ROT_AXIS_UP_ANGLE] = rData[ROT_ACCUM_X];
        rData[ROT_AXIS_RT_ANGLE] = rData[ROT_ACCUM_Y];

        Quat.fromAxisAngle(rData, ROT_X, rData, ROT_AXIS_UP, true);
        Quat.fromAxisAngle(rData, ROT_Y, rData, ROT_AXIS_RT, true);


        Quat.mulQ(rData, ROT_XY, rData, ROT_X, rData, ROT_Y);
        Quat.mulQ(rotCurrent, 0, rData, ROT_XY, rotInitial, 0);

        Quat.localRight(rotLocRt, 0, rotCurrent, 0);
        Quat.localForward(rotLocFw, 0, rotCurrent, 0);

        mTransform.setRotation(rotCurrent);
    }

    @Override
    public void onKeyInput(int key, int action) {
        if (action != GLFW_PRESS && action != GLFW_RELEASE) return;

        mData[MOV_INPUT_VALUE] = action == GLFW_RELEASE ? -1 : 1;
        boolean inputVectorChanged = true;
        switch (key) {
            default: inputVectorChanged = false;
            case GLFW_KEY_LEFT_SHIFT:
                mData[MOV_SPEED] = action == GLFW_RELEASE ? WALK_SPEED : SPRINT_SPEED;
            break;
            case GLFW_KEY_W: mData[MOV_INPUT_VEC_Z] -= mData[MOV_INPUT_VALUE]; break;
            case GLFW_KEY_A: mData[MOV_INPUT_VEC_X] -= mData[MOV_INPUT_VALUE]; break;
            case GLFW_KEY_S: mData[MOV_INPUT_VEC_Z] += mData[MOV_INPUT_VALUE]; break;
            case GLFW_KEY_D: mData[MOV_INPUT_VEC_X] += mData[MOV_INPUT_VALUE]; break;
        }

        if (inputVectorChanged) {
            Vec3.mag(mData, MOV_INPUT_MAG, mData, MOV_INPUT_VEC);
        }
    }

    @Override
    public void onUpdate(float timeStep) {
        if (mData[MOV_INPUT_MAG] > 0) {
            Vec3.div(mData, MOV_VECTOR, mData, MOV_INPUT_VEC, mData[MOV_INPUT_MAG]);
            Vec3.mul(mData, MOV_VECTOR, mData, MOV_VECTOR, mData[MOV_SPEED] * timeStep);

            Vec3.mul(mData, MOV_STRAFE_OFFSET,  rotLocRt, 0, mData[MOV_VECTOR_X]);
            Vec3.mul(mData, MOV_FORWARD_OFFSET, rotLocFw, 0, mData[MOV_VECTOR_Z]);

            mTransform.getPosition(posCurrent);

            Vec3.add(posCurrent, 0, posCurrent, 0, mData, MOV_STRAFE_OFFSET);
            Vec3.add(posCurrent, 0, posCurrent, 0, mData, MOV_FORWARD_OFFSET);

            mTransform.setPosition(posCurrent);
        }
    }
}










































