package ca.hex.engine.old;

import java.util.ArrayList;

public class Entity {

    private ArrayList<Component> mComponents = new ArrayList<>();

    private Transform mTransform = null;

    public void addComponent(Class<? extends Component> componentClass) {
        Component component = Component.create(componentClass, this);
        if (component == null) return;

        mComponents.add(component);
        if (component instanceof Transform) {
            mTransform = (Transform) component;
        }
    }

    public void destroyComponent(Component component) {
        if (mComponents.contains(component)) {
            mComponents.remove(component);
            component.onDestroy();
        }
    }

    public void addChild(Entity entity) {
        mTransform.addChild(entity.mTransform);
    }

    public void removeChild(Entity entity) {
        mTransform.removeChild(entity.mTransform);
    }

    public Transform transform() {
        return mTransform;
    }

    @SafeVarargs
    public Entity(Class<? extends Component>... components) {
        for (Class<? extends Component> component : components) {
            addComponent(component);
        }
        if (mTransform == null) {
            //add the required transform component
            addComponent(Transform.class);
        }
    }

    public void destroy() {
        mTransform.onDestroy();
    }

    public void onInitialize() {
        mComponents.forEach(component -> component.onInitialize());
    }

    public void onResize(int width, int height) {
        mComponents.forEach(component -> component.onResize(width, height));
    }

    public void onKeyInput(int key, int action) {
        mComponents.forEach(component -> component.onKeyInput(key, action));

    }

    public void onMouseInput(float deltaX, float deltaY) {
        mComponents.forEach(component -> component.onMouseInput(deltaX, deltaY));

    }

    public void onUpdate(float timeStep) {
        mComponents.forEach(component -> component.onUpdate(timeStep));
    }

    public void onRender(float alpha) {
        mComponents.forEach(component -> component.onRender(alpha));
    }
}
