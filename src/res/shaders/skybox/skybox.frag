#version 400 core

uniform samplerCube uSkycube;

in vec3 vEyeDir;

out vec4 outColor;

void main(void) {
    outColor = texture(uSkycube, vEyeDir);
}