#version 400 core

uniform mat4 uInvModelView;
uniform mat4 uInvProj;

in vec4 aPosition;

out vec3 vEyeDir;

void main(void) {
    vEyeDir = mat3(uInvModelView) * (uInvProj * aPosition).xyz;
    gl_Position = aPosition;
}