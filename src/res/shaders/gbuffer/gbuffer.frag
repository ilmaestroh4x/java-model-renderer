#version 400 core

uniform float uOpacity;
uniform float uShininess;
uniform float uShininessStrength;
uniform float uReflectivity;
uniform float uRefractionIndex;

uniform vec4 uDiffuseColor;
uniform vec4 uAmbientColor;
uniform vec4 uSpecularColor;
uniform vec4 uEmissiveColor;
uniform vec4 uTransparentColor;
uniform vec4 uReflectiveColor;

uniform sampler2D uDiffuseTexture;
uniform sampler2D uSpecularTexture;
uniform sampler2D uAmbientTexture;
uniform sampler2D uEmissiveTexture;
uniform sampler2D uHeightTexture;
uniform sampler2D uNormalTexture;
uniform sampler2D uShininessTexture;

uniform samplerCube uEnvironmentCubemap;

in VERT_OUT {
    mat3 TBN;
    vec3 Position;
    vec3 Normal;
    vec2 TexCoord;
} fragIn;

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoRoughness;
layout (location = 3) out vec4 gEmissionMetallic;
//layout (location = 4) out vec4 gShadow;

vec3 textureN(in sampler2D normalMap, in vec2 texCoord, in mat3 TBN);

void main(void) {
    gPosition = fragIn.Position;
    gNormal = textureN(uNormalTexture, fragIn.TexCoord, fragIn.TBN).xyz;
    gAlbedoRoughness.rgb = texture(uDiffuseTexture, fragIn.TexCoord).rgb;
    gAlbedoRoughness.a = texture(uShininessTexture, fragIn.TexCoord).x;
    gEmissionMetallic.rgb = texture(uEmissiveTexture, fragIn.TexCoord).rgb;
    gEmissionMetallic.a = uShininess;
    //gShadow = vec4(1.0);
}

vec3 textureN(in sampler2D normalMap, in vec2 texCoord, in mat3 TBN) {
    vec3 normal = normalize(texture(normalMap, texCoord).xyz * 2.0 - 1.0);
    return normalize(TBN * normal);
}
