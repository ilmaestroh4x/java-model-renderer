#version 400 core

#define MAX_BONES 100

//Shader Uniforms (read-only globals)
uniform mat4 uModel;
uniform mat4 uInvModel;

uniform mat4 uModelViewProjection;

uniform bool uHasBones;
uniform mat4 uBones[MAX_BONES];

//Mesh Attributes
in vec3 aPosition;
in vec3 aNormal;
in vec3 aTangent;
//in vec3 aBiTangent;
in vec4 aColor;
in vec2 aTexCoord;

in  vec4 aWeights;
in ivec4 aBoneIds;

//Vertex Shader Output
out VERT_OUT {
    mat3 TBN;
    vec3 Position;
    vec3 Normal;
    vec2 TexCoord;
} vsOut;

mat4 calcSkin(in ivec4 boneIds, in vec4 boneWeights);

mat3 calcTBN(in mat4 normalMatrix, in vec4 normal, in vec4 tangent);

void main(void) {
    vec4 position   = vec4(aPosition,   1.0);
    vec4 normal     = vec4(aNormal,     0.0);
    vec4 tangent    = vec4(aTangent,    0.0);

    if (uHasBones) {
        mat4 skinMat = calcSkin(aBoneIds, aWeights);
        position    = skinMat * position;
        normal      = skinMat * normal;
        tangent     = skinMat * tangent;
    }

    mat4 normalMatrix = transpose(uInvModel);

    mat3 TBN = calcTBN(normalMatrix, normal, tangent);

    vec3 worldPosition = (uModel * position).xyz;
    vec3 worldNormal = normalize((normalMatrix * normal).xyz);

    vsOut.TBN       = TBN;
    vsOut.Position  = worldPosition;
    vsOut.Normal    = worldNormal;
    vsOut.TexCoord  = aTexCoord;

    gl_Position = uModelViewProjection * position;
}

mat4 calcSkin(in ivec4 boneIds, in vec4 boneWeights) {
    return (
        uBones[boneIds[0]] * boneWeights[0] +
        uBones[boneIds[1]] * boneWeights[1] +
        uBones[boneIds[2]] * boneWeights[2] +
        uBones[boneIds[3]] * boneWeights[3]
    );
}

mat3 calcTBN(in mat4 normalMatrix, in vec4 normal, in vec4 tangent) {
    vec3 T = normalize((normalMatrix * tangent).xyz);
    vec3 N = normalize((normalMatrix * normal).xyz);
    //Gram–Schmidt process
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(T, N);
    return mat3(T, B, N);
}