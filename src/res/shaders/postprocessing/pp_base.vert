#version 400 core

in vec3 aPosition;
in vec2 aTexCoord;

out vec2 vTexCoord;

void main(void) {
    vTexCoord = aTexCoord;
    gl_Position = vec4(aPosition.xy, 0.0, 1.0);
}