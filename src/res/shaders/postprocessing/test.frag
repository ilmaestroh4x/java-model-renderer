#version 400 core

uniform sampler2D uPosition;
uniform sampler2D uNormal;
uniform sampler2D uAlbedoRoughness;
uniform sampler2D uEmissionMetallic;
uniform sampler2D shadowMap;

in vec2 vTexCoord;

out vec4 outColor;

float linearizeDepth(vec2 uv) {
  float n = 0.1; // camera z near
  float f = 100.0; // camera z far
  float z = texture(shadowMap, uv).x;
  return (2.0 * n) / (f + n - z * (f - n));
}

void main() {
    outColor = vec4(texture(shadowMap, vTexCoord).xyz, 1.0);
/*
    vec2 dbguv = clamp(fract(vTexCoord * 2.0), 0.0, 1.0);
    outColor = vec4(dbguv, 0.0, 1.0);

    if (vTexCoord.x > 0.5 && vTexCoord.y > 0.5) {
        outColor = vec4(texture(uAlbedoRoughness, dbguv).rgb, 1.0);
    } else if (vTexCoord.x > 0.5) {
        outColor = vec4(texture(uNormal, dbguv).xyz, 1.0);
    } else if (vTexCoord.y > 0.5) {
        outColor = vec4(vec3(texture(uAlbedoRoughness, dbguv).a), 1.0);
    } else {
        outColor = vec4(texture(uEmissionMetallic, dbguv).rgb, 1.0);
    }
*/
}
