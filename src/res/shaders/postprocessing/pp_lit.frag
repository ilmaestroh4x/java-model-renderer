#version 400 core

#define MAX_DIRECTIONAL_LIGHTS 5
#define MAX_POINT_LIGHTS 5
#define MAX_SPOT_LIGHTS 5

struct DirectionalLight {
    vec3 Direction;
    vec3 Color;
};

struct PointLight {
    vec3 Position;
    vec3 Color;
};

struct SpotLight {
    vec3 Position;
    vec3 Direction;
    vec3 Color;
};

uniform int uDirectionalLightsActive;
uniform int uPointLightsActive;
uniform int uSpotLightsActive;

uniform DirectionalLight uDirLights[MAX_DIRECTIONAL_LIGHTS];
uniform PointLight uPointLights[MAX_POINT_LIGHTS];
uniform SpotLight uSpotLights[MAX_SPOT_LIGHTS];

uniform vec3 uViewPosition;

uniform sampler2D uPosition;
uniform sampler2D uNormal;
uniform sampler2D uAlbedoRoughness;
uniform sampler2D uEmissionMetallic;

in vec2 vTexCoord;

out vec4 outColor;

struct Surface {
    vec3 Position;
    vec3 Normal;
    vec3 Albedo;
    vec3 Emission;
    float Roughness;
    float Metalness;
};

vec3 calcDirLight(in DirectionalLight dirLight, in Surface surface, in vec3 viewDirection);
vec3 calcPointLight(in PointLight pointLight, in Surface surface, in vec3 viewDirection);

float ShadowCalculation(in vec4 fragPosLightSpace);

uniform mat4 testShadowSpaceMatrix;
uniform sampler2D shadowMap;
const vec3 testShadowCaster = vec3(-4.0, 10.0, -1.0);

void main() {
    vec4 albedoRoughness = texture(uAlbedoRoughness, vTexCoord);
    vec4 emissionMetallic = texture(uEmissionMetallic, vTexCoord);

    Surface surface;
    surface.Position = texture(uPosition, vTexCoord).xyz;
    surface.Normal = texture(uNormal, vTexCoord).xyz;
    surface.Albedo = albedoRoughness.rgb;
    surface.Emission = emissionMetallic.rgb;
    surface.Roughness = albedoRoughness.a;
    surface.Metalness = clamp(emissionMetallic.a, 0.05, 1.0);

    vec3 viewDir = normalize(uViewPosition - surface.Position);
    float viewDist = length(uViewPosition - surface.Position);



    vec3 color = surface.Albedo * 0.01;

    for (int i = 0; i < uDirectionalLightsActive; i++) {
        color += calcDirLight(uDirLights[i], surface, viewDir);
    }

    for (int i = 0; i < uPointLightsActive; i++) {
        color += calcPointLight(uPointLights[i], surface, viewDir);
    }

    float shadow = 1.0 - ShadowCalculation(testShadowSpaceMatrix * vec4(surface.Position, 1.0));
    //color *= shadow;

    //color = max(color, surface.Emission);

    //Apply gamma correction
    float gamma = 2.2;
    color = pow(color, vec3(1.0 / gamma));

    outColor = vec4(color, 1.0);
}

vec3 calcDirLight(in DirectionalLight dirLight, in Surface surface, in vec3 viewDirection) {
    vec3 lightDir = normalize(-dirLight.Direction);

    float diffuse = max(dot(surface.Normal, lightDir), 0.0);

    vec3 reflectDir = reflect(-lightDir, surface.Normal);
    float specular = pow(max(dot(viewDirection, reflectDir), 0.0), surface.Metalness * 64.0) * (1.0 - surface.Roughness);

    return (diffuse + specular) * surface.Albedo * dirLight.Color;
}

vec3 calcPointLight(in PointLight pointLight, in Surface surface, in vec3 viewDirection) {
    vec3 lightDir = normalize(pointLight.Position - surface.Position);
    float lightDist = length(pointLight.Position - surface.Position);

    float attenuation = 1.0 / lightDist; //linear lighting (because of gamma correction)

    float diffuse = max(dot(surface.Normal, lightDir), 0.0);

    vec3 reflectDir = reflect(-lightDir, surface.Normal);
    float specular = pow(max(dot(viewDirection, reflectDir), 0.0), surface.Metalness * 64.0) * (1.0 - surface.Roughness);

    return (diffuse + specular) * attenuation * surface.Albedo * pointLight.Color;
}

float ShadowCalculation(in vec4 fragPosLightSpace) {
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
    float bias = 0.005;
    float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;

    return shadow;
}