#version 400 core

#define MAX_BONES 100

//Shader Uniforms (read-only globals)
uniform mat4 uModel;
uniform mat4 uInvModel;

uniform mat4 uModelViewProjection;

uniform bool uHasBones;
uniform mat4 uBones[MAX_BONES];

//Mesh Attributes
in vec3 aPosition;

in  vec4 aWeights;
in ivec4 aBoneIds;

mat4 calcSkin(in ivec4 boneIds, in vec4 boneWeights);

void main(void) {
    vec4 position   = vec4(aPosition,   1.0);

    if (uHasBones) {
        mat4 skinMat = calcSkin(aBoneIds, aWeights);
        position    = skinMat * position;
    }

    gl_Position = uModelViewProjection * position;
}

mat4 calcSkin(in ivec4 boneIds, in vec4 boneWeights) {
    return (
        uBones[boneIds[0]] * boneWeights[0] +
        uBones[boneIds[1]] * boneWeights[1] +
        uBones[boneIds[2]] * boneWeights[2] +
        uBones[boneIds[3]] * boneWeights[3]
    );
}