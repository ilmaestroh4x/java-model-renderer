# Java Model Renderer

![](preview_javamodelviewer.gif)

## Overview
This project was created to explore rendering 3d models using OpenGL (lwjgl wrapper) in Java.

Some of the features include directional and point lights, normal mapping, and animation. (Both are GPU bone based skinning and hard surface transform based animations).

## Getting started
The following instructions will get you up and running to try this project out on your own system.

### Prerequisites
* [Java JDK](https://www.oracle.com/ca-en/java/technologies/javase-downloads.html) - (required) The project will need to be built on your own system, so the JRE is not sufficient.
* [git](https://git-scm.com/) - (optional) If you wish to clone this project, you can also just download the repo instead.
* [Intellij IDEA](https://www.jetbrains.com/idea/) - (required) You don't need this. However, running the project without it is outside the scope of this project. (The free community edition will work perfectly for this.)
* 
If using git, you can use the command:
```
git clone https://gitlab.com/ilmaestroh4x/java-model-renderer.git
```
### Running project
Once the project is downloaded and the above dependancies are installed, simply open the project in intellij, open the `main.java` file under `src\ca\hex\` and click the `Run` button at the top of the IDE.
This will load up the default scene.

Controls:
 * ESC - Exit
 * W,A,S,D - Move (Hold shift to sprint)
 * Mouse - Look around
 * Spacebar - Toggle animation playback
 * Enter - Adds randomly placed directional and point lights to the scene

## License
See 'LICENSE' in project root.